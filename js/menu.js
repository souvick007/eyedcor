/**
 * Created by developer15 on 4/1/17.
 */
function openNav() {
    document.getElementById("mySidenavs").style.width = "250px";
    /**Add below code for overlay effect**/
    var div = document.createElement("div");
    div.setAttribute("class", "eth-overlay-Menu" );
    document.body.appendChild(div);
    /** End Add**/
}

function closeNav() {
    document.getElementById("mySidenavs").style.width = "0";
    /**Add below code for overlay effect**/
    var elem = document.getElementsByClassName("eth-overlay-Menu");
    elem[0].outerHTML = "";
}