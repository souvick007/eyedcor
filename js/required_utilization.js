/*outsource onclick function for update*/

/*Function for to print the bill*/

var chckboxval;


function outsource(val){
    var ord_stat = $("#order_status"+val).val();
    window.location.href = window.location.origin + "/eyedcor/wholesale/outsource_update?item_id="+val+"&stat="+ord_stat;
}

function returnvalinvoice() {


    var invoice_no = $("#invoice_no").val();

    var indv_checkbox = $('#delivcheckboxs'+chckboxval).prop("checked");
    var sel_all_checkbox = $('#select_all').prop("checked");
    var chcking_pymnt = $('#paymntchck').prop('checked');


    if(invoice_no == ""){
        BootstrapDialog.alert('Please Enter Inv No.');
        return false;
    }

    if(indv_checkbox == true || chcking_pymnt == true || sel_all_checkbox == true){
        //return true;
    }else{
        BootstrapDialog.alert('Please Select Checkbox');
        return false;
    }
}

function searchlist(){

    var prod_name=$('#prod_name').val();
    var sph=$('#sph').val();
    var cyl=$('#cyl').val();
    var axis=$('#axis').val();
    var addition=$('#addition').val();


    $.ajax({
        type:'POST',
        url:window.location.origin+'/eyedcor/wholesale/stocklistfetchspec',
        data:{prod_name :prod_name,sph:sph,cyl:cyl,axis:axis,addition:addition},
        beforeSend: function(){
            $('#myDiv').show();
        },
        success:function(result){
            console.log(result);
            $('#myDiv').hide();
            $("#gettingstocklist").html(result).show();
        }
    });
}

var arr=[];

function chckbxval(val){

    chckboxval=val;
}


$(document).ready(function(){

    var url=$(location).attr('pathname');;
    var segments = url.split( '/' );
    var action = segments[3];
    console.log("getting segment value");
    console.log(segments[3]);

    $("#select_all").change(function(){  //"select all" change
        var status = this.checked; // "select all" checked status
        $('.checkbox1').each(function(){ //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
    });

    //Notification Box
    $("#notification").fadeIn("slow").delay(3000).fadeOut("slow");


    //Datatable for purchase order page
    $('#admindelivlist').DataTable(
        {
            "aaSorting": [],
            "scrollY": "300px",
            "fixedHeader": {
                "header": true
            },
            language : {
                sLengthMenu: "Show _MENU_"
            },

            "dom": '<"top"f>rt<"clear">',
            buttons: [{
                extend: 'excelHtml5',
                customize: function(xlsx) {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];

                    // Loop over the cells in column `F`
                    $('row c[r^="F"]', sheet).each( function () {
                        // Get the value and strip the non numeric characters
                        if ( $('is t', this).text().replace(/[^\d]/g, '') * 1 >= 500000 ) {
                            $(this).attr( 's', '20' );
                        }
                    });
                }
            }]
        }
    );


    if(segments[3] == "stocklist"){
        $.ajax({
            type:'POST',
            url:window.location.origin+'/eyedcor/wholesale/stocklistfetch',
            beforeSend: function(){
                $('#myDiv').show();
            },
            success:function(result){
                console.log(result);
                $('#myDiv').hide();
                $("#gettingstocklist").html(result).show();
            }
        });
    }


});

