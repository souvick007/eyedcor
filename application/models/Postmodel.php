
<?php
/**
 * @author Subhajit
 * @copyright 2016
 */
class Postmodel extends CI_Model
{

    function Model()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_patient_lens_info($data){

        $p0=$data['prescription_id'];
//        print_r($p0);exit();

        $this->db->query("SET @p0 := '$p0';");

        $success = $this->db->query("CALL get_patient_lens_info(@p0);");

        $result=$success->result();
//        print_r($success);exit();

        return $result;

    }




    function save_appointment_info($data){
        $p0=$data['appointment_id'];
        $p1=$data['doctor_in'];
        $p2=$data['patient_id'];
        $p3=$data['created_by'];
        $p4=$data['appointment_on'];

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $this->db->query("SET @p2 := '$p2';");
        $this->db->query("SET @p3 := '$p3';");
        $this->db->query("SET @p4 := '$p4';");

        $success = $this->db->query("CALL save_appointment_info(@p0,@p1,@p2,@p3,@p4);");

//        $result = $success->result();

//        print_r($result);exit();

        return 0;
    }

    function save_update_prescription($data){

//        print_r($data);exit();
        $p0 = $data['prescription_id'];
        $p1 = $data['patient_id'];
        $p2 = $data['prescribed_by'];
        $p3 = $data['other_info'];

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $this->db->query("SET @p2 := '$p2';");
        $this->db->query("SET @p3 := '$p3';");

        $success = $this->db->query("CALL save_update_prescription(@p0,@p1,@p2,@p3);");

        $out_param_query = $this->db->query('SELECT @p0 AS  `prescription_id_in`');
        $result = $out_param_query->result();

//        print_r($result);exit();
        return $result;

    }

    function save_update_patient_lense_info1($data){
//        print_r($data);exit();
        $p0 = $data['prescription_lens_id'];
        $p1 = $data['prescription_id'];
        $p2 = $data['eye_type'];
        $p3 = $data['measurement_type'];
        $p4 = $data['r_d_sph'];
        $p5 = $data['r_d_cyl'];
        $p6 = $data['r_d_axis'];
        $p7 = $data['r_d_va'];
        $p8 = $data['created_by'];

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $this->db->query("SET @p2 := '$p2';");
        $this->db->query("SET @p3 := '$p3';");
        $this->db->query("SET @p4 := '$p4';");
        $this->db->query("SET @p5 := '$p5';");
        $this->db->query("SET @p6 := '$p6';");
        $this->db->query("SET @p7 := '$p7';");
        $this->db->query("SET @p8 := '$p8';");

//        print_r($this->db->query);exit();

        $success = $this->db->query("CALL save_update_patient_lense_info(@p0,@p1,@p2,@p3,@p4,@p5,@p6,@p7,@p8);");

        $out_param_query = $this->db->query('SELECT @p0 AS  `prescribed_lense_id_in`');

        $result = $out_param_query->result();

//        print_r($result);exit();

        return $result;
    }

    function save_update_patient_lense_info2($data){
//        print_r($data);exit();

        $p0 = $data['prescription_lens_id'];
        $p1 = $data['prescription_id'];
        $p2 = $data['eye_type'];
        $p3 = $data['measurement_type'];
        $p4 = $data['r_n_sph'];
        $p5 = $data['r_n_cyl'];
        $p6 = $data['r_n_axis'];
        $p7 = $data['r_n_va'];
        $p8 = $data['created_by'];

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $this->db->query("SET @p2 := '$p2';");
        $this->db->query("SET @p3 := '$p3';");
        $this->db->query("SET @p4 := '$p4';");
        $this->db->query("SET @p5 := '$p5';");
        $this->db->query("SET @p6 := '$p6';");
        $this->db->query("SET @p7 := '$p7';");
        $this->db->query("SET @p8 := '$p8';");

//        print_r($this->db->query);exit();

        $success = $this->db->query("CALL save_update_patient_lense_info(@p0,@p1,@p2,@p3,@p4,@p5,@p6,@p7,@p8);");

        $out_param_query = $this->db->query('SELECT @p0 AS  `prescribed_lense_id_in`');

        $result = $out_param_query->result();

//        print_r($result);exit();

        return $result;
    }

    function save_update_patient_lense_info3($data){
//        print_r($data);exit();

        $p0 = $data['prescription_lens_id'];
        $p1 = $data['prescription_id'];
        $p2 = $data['eye_type'];
        $p3 = $data['measurement_type'];
        $p4 = $data['l_d_sph'];
        $p5 = $data['l_d_cyl'];
        $p6 = $data['l_d_axis'];
        $p7 = $data['l_d_va'];
        $p8 = $data['created_by'];

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $this->db->query("SET @p2 := '$p2';");
        $this->db->query("SET @p3 := '$p3';");
        $this->db->query("SET @p4 := '$p4';");
        $this->db->query("SET @p5 := '$p5';");
        $this->db->query("SET @p6 := '$p6';");
        $this->db->query("SET @p7 := '$p7';");
        $this->db->query("SET @p8 := '$p8';");

//        print_r($this->db->query);exit();

        $success = $this->db->query("CALL save_update_patient_lense_info(@p0,@p1,@p2,@p3,@p4,@p5,@p6,@p7,@p8);");

        $out_param_query = $this->db->query('SELECT @p0 AS  `prescribed_lense_id_in`');

        $result = $out_param_query->result();

//        print_r($result);exit();

        return $result;
    }

    function save_update_patient_lense_info4($data){
//        print_r($data);exit();

        $p0 = $data['prescription_lens_id'];
        $p1 = $data['prescription_id'];
        $p2 = $data['eye_type'];
        $p3 = $data['measurement_type'];
        $p4 = $data['l_n_sph'];
        $p5 = $data['l_n_cyl'];
        $p6 = $data['l_n_axis'];
        $p7 = $data['l_n_va'];
        $p8 = $data['created_by'];

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $this->db->query("SET @p2 := '$p2';");
        $this->db->query("SET @p3 := '$p3';");
        $this->db->query("SET @p4 := '$p4';");
        $this->db->query("SET @p5 := '$p5';");
        $this->db->query("SET @p6 := '$p6';");
        $this->db->query("SET @p7 := '$p7';");
        $this->db->query("SET @p8 := '$p8';");

//        print_r($this->db->query);exit();

        $success = $this->db->query("CALL save_update_patient_lense_info(@p0,@p1,@p2,@p3,@p4,@p5,@p6,@p7,@p8);");

        $out_param_query = $this->db->query('SELECT @p0 AS  `prescribed_lense_id_in`');

        $result = $out_param_query->result();

//        print_r($result);exit();

        return $result;
    }

    function save_update_patient_info($data)
    {
        $p0 = $data['patient_id'];
        $p1 = $data['patient_name'];
        $p2 = $data['dob'];
        $p3 = $data['cur_age'];
        $p4 = $data['sex'];
        $p5 = $data['mobile_numb'];
        $p6 = $data['alt_mobile_numb'];
        $p7 = $data['address'];
        $p8 = $data['patient_status'];
        $p9 = $data['deliv_status'];
        $p10 = $data['back_ofc_status'];
        $p11 = $data['pic_url'];
        $p12 = $data['created_by'];
        $p13 = $data['doctor_in'];

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $this->db->query("SET @p2 := '$p2';");
        $this->db->query("SET @p3 := '$p3';");
        $this->db->query("SET @p4 := '$p4';");
        $this->db->query("SET @p5 := '$p5';");
        $this->db->query("SET @p6 := '$p6';");
        $this->db->query("SET @p7 := '$p7';");
        $this->db->query("SET @p8 := '$p8';");
        $this->db->query("SET @p9 := '$p9';");
        $this->db->query("SET @p10 := '$p10';");
        $this->db->query("SET @p11 := '$p11';");
        $this->db->query("SET @p12 := '$p12';");
        $this->db->query("SET @p13 := '$p13';");
//        print_r($this->db->query);exit();


        $success = $this->db->query("CALL save_update_patient_info(@p0,@p1,@p2,@p3,@p4,@p5,@p6,@p7,@p8,@p9,@p10,@p11,@p12,@p13);");

        $out_param_query = $this->db->query('SELECT @p0 AS  `patient_id_in`');

        $result = $out_param_query->result();

        return $result;
    }
    function get_all_doctor_name()
    {
        $success = $this->db->query("CALL get_all_doctor_name();");
        $result=$success->result();
//        print_r($result);exit();
        return $result;
    }

    //specific patient list
    function get_patient_details($data)
    {
//        print_r($data);exit();
        $p0 = $data['patient_id'];
        $p1 = $data['off_set'];
        $p2 = $data['limit'];

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $this->db->query("SET @p2 := '$p2';");
        $success = $this->db->query("CALL get_patient_details(@p0,@p1,@p2,@p3);");

//        $out_param_query = $this->db->query('select @p3 AS  `totalRowCnt`;');
        $result=$success->result();
//        print_r($result);exit();
        return $result;
    }

    //all patient list
    function get_patient_details1(){
        $p0 ="";
        $p1 =0;
        $p2 = 20;

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $this->db->query("SET @p2 := '$p2';");
        $success = $this->db->query("CALL get_patient_details(@p0,@p1,@p2,@p3);");

//        $out_param_query = $this->db->query('select @p3 AS  `totalRowCnt`;');
        $result=$success->result();
//        print_r($result);exit();
        return $result;
    }

    //For Dr Details page

    function get_prescription_info($data){
//        print_r($data);exit();

        $p0 = $data['patient_id'];
        $p1 = $data['prescription_id'];
        $p2 = $data['offset'];
        $p3 = $data['limit'];

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $this->db->query("SET @p2 := '$p2';");
        $this->db->query("SET @p3 := '$p3';");
        $success = $this->db->query("CALL get_prescription_info(@p0,@p1,@p2,@p3,@p4);");

        $result=$success->result();
//        print_r($result);exit();
        return $result;
    }

    function get_prescription_info1(){
        $p0 ="";
        $p1 = "";
        $p2 = 0;
        $p3 = 100;

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $this->db->query("SET @p2 := '$p2';");
        $this->db->query("SET @p3 := '$p3';");
        $success = $this->db->query("CALL get_prescription_info(@p0,@p1,@p2,@p3,@p4);");

        $result=$success->result();
//        print_r($result);exit();
        return $result;
    }

    function get_country_list(){

        $success = $this->db->query("CALL get_country_list();");
        print_r($success->result());exit();

    }


    //Wholesale part
    function save_update_sales_order($data){

        $p0=$data['order_id'];
        $p1=$data['bill_no'];
        $p2=$data['party_name'];
        $p3=$data['date'];
        $p4=$data['order_status'];

        $this->db->query("SET @p0 := '$p0';");

        $this->db->query("CALL save_update_sales_order(@p0 ,'$p1','$p2','$p3','$p4');");


        $out_param_query = $this->db->query('SELECT @p0 AS  `order_id`');
        $result = $out_param_query->result();

//        print_r($result);exit();
        return $result;
    }

    function save_update_sales_order_item($data){
//        print_r("Hello");exit();
        $p0=$data['item_id'];
        $p1=$data['order_id'];
        $p2=$data['item_ref_no'];
        $p3=$data['prod_type'];
        $p4=$data['prod_name'];
        $p5=$data['comp_name'];
        $p6=$data['add_type'];
        $p7=$data['specification'];
        $p8=$data['sph'];
        $p9=$data['cyl'];
        $p10=$data['axis'];
        $p11=$data['addition'];
        $p12=$data['dia'];
        $p13=$data['base'];
        $p14=$data['side'];
        $p15=$data['quant'];
        $p16=$data['remarks'];
        $p17=$data['order_status'];
        $p18=$data['frame'];
        $p19=$data['lens'];
        $p20=$data['supplier_name'];
        $p21=$data['item_price'];

        $this->db->query("SET @p0 := '$p0';");

//        print_r("CALL save_update_sales_order_item(@p0,'$p1','$p2','$p3','$p4','$p5','$p6','$p7','$p8','$p9','$p10','$p11','$p12','$p13','$p14','$p15','$p16','$p17','$p18','$p19','$p20','$p21');");exit();

        $this->db->query("CALL save_update_sales_order_item(@p0,'$p1','$p2','$p3','$p4','$p5','$p6','$p7','$p8','$p9','$p10','$p11','$p12','$p13','$p14','$p15','$p16','$p17','$p18','$p19','$p20','$p21');");

        $out_param_query = $this->db->query('SELECT @p0 AS  `item_id`');
        $result = $out_param_query->result();

//        print_r($result);exit();
        return $result;

    }

//    function insertitem(){
//
//        $this->db->query(
//            'INSERT INTO sales_order_items (
//sales_order_id,
//  ref_no,
//  product_type,
//  product_name,
//  company_name,
//  add_type,
//  specification,
//  sph,
//  cyl,
//  axis,
//  addition,
//  diameter,
//  base,
//  side,
//  frame,
//  lens,
//  supplier_name,
//  remarks,
//  status,
//  price,
//  ) VALUES (?, ?, ?, ?,?, ?, ?, ?,?, ?, ?, ?)',
//            array($a, $b, $c, $d)
//        );
//        $this->db->query('INSERT INTO `sales_order_items`
//	(`sales_order_id`,
//	`ref_no`,
//	`product_type`,
//	`product_name`,
//	`company_name`,
//	`add_type`,
//	`specification`,
//	`sph`,
//	`cyl`,
//	`axis`,
//	`addition`,
//	`diameter`,
//	`base`,
//	`side`,
//    `frame`,
//    `lens`,
//    `supplier_name`,
//	`remarks`,
//	`status`,
//	`price`)
//	VALUES
//    (sales_order_id,
//        ref_no_in,
//        product_type,
//        product_name,
//        company_name,
//        add_type_in,
//        specification,
//        sph,
//        cyl,
//        axis,
//        addition,
//        diameter,
//        base,
//        side,
//        frame,
//        lens,
//        quantity,
//        supplier_name,
//        remarks,
//        status,
//        price_in);');
//    }
/*functionality added for bulk purchase*/

    function get_sales_order_item_of_bulk_purchase($data){


        $p0=$data['sales_order_id'];
        $p1=$data['order_status'];
        $p2=$data['offset'];
        $p3=$data['limit'];

        $success=$this->db->query("CALL get_sales_order_item('$p0','$p1','$p2','$p3','@p4');");

        $result=$success->result();
//        print_r($result);exit();
        return $result;

    }





    function get_sales_order_item($data){

        $p0=$data['sales_order_id'];
        $p1=$data['order_status'];
        $p2=$data['offset'];
        $p3=$data['limit'];

        $success=$this->db->query("CALL get_sales_order_item('$p0','$p1','$p2','$p3','@p4');");

        $result=$success->result();
//        print_r($result);exit();
        return $result;
    }

    function get_sales_order_item1($data){

//        print_r($data);exit();
        $p0=$data['sales_order_id'];
        $p1=$data['order_status'];
        $p2=$data['offset'];
        $p3=$data['limit'];

        $success=$this->db->query("CALL get_sales_order_item('$p0','$p1','$p2','$p3','@p4');");

        $result=$success->result();
//        print_r($result);exit();
        return $result;

    }


    function get_sales_order_item_payment($data){
        $query = $this->db->query("select o.id, o.sales_order_id,o.bill_no, o.party_name,i.item_id,o.order_date,i.product_type,i.product_name,i.company_name,i.specification,i.sph,i.cyl,i.axis,i.addition,i.diameter,i.base,i.side,i.quantity,i.remarks,i.status,i.price from sales_order o left join sales_order_items i on o.id = i.sales_order_id where i.item_id in($data)");
        $result = $query->result();
        return $result;
    }

    function add_to_stock($data){
        $p0=$data['prod_name'];
        $p1=$data['sph'];
        $p2=$data['cyl'];
        $p3=$data['axis'];
        $p4=$data['addition'];
        $p5=$data['quant'];

//        print_r("CALL add_to_stock('$p0','$p1','$p2','$p3','$p4','$p5');");exit();

        $success=$this->db->query("CALL add_to_stock('$p0','$p1','$p2','$p3','$p4','$p5');");
        return 0;
    }

    function remove_from_stock($data){

//        print_r($data);exit();

        $p0=$data['prod_name'];
        $p1=$data['sph'];
        $p2=$data['cyl'];
        $p3=$data['axis'];
        $p4=$data['addition'];
        $p5=$data['quant'];

        $success=$this->db->query("CALL remove_from_stock('$p0','$p1','$p2','$p3','$p4','$p5');");
        return 0;
    }

    function get_stock_list(){

        $p0=0;
        $p1=10000;

        $this->db->query("SET @p0 := '$p0';");
        $this->db->query("SET @p1 := '$p1';");
        $success = $this->db->query("CALL get_stock_list(@p0,@p1,@p2);");

        $result=$success->result();

//        print_r($result);exit();
        return $result;
    }

function update_stat($id,$stat,$manf,$purchase_price){
    $query = $this->db->query("update sales_order_items set status = ?,manf_ref_no=? purchase_price=? where item_id = ? ", array('status'=>$stat,'manf'=>$manf,'item_id'=>$id,'purchase_price'=>$purchase_price));

    return true;
}

    function save_payment($data){

        $p0=$data['paymnt_wholesale_id'];
        $p1=$data['inv_no'];
        $p2=$data['ord_no'];
        $p3=$data['bill_no'];
        $p4=$data['prty_name'];
        $p5=$data['item_id'];
        $p6=$data['indvprice'];
        $p7=$data['amount'];
        $p8=$data['ser_tax'];
        $p9=$data['tot_amnt'];
        $p10=$data['payed'];
        $p11=$data['due_amount'];
        $p12=$data['updated_by'];
        $p13=$data['payment_type'];
        $p14=$data['payment_type_id'];

        $this->db->query("SET @p0 := '$p0';");
//        print_r("CALL save_update_payment_wholesale(@p0,'$p1','$p2','$p3','$p4','$p5','$p6','$p7','$p8','$p9','$p10');");exit();

        $success=$this->db->query("CALL save_update_payment_wholesale(@p0,'$p1','$p2','$p3','$p4','$p5','$p6','$p7','$p8','$p9','$p10','$p11','$p12','$p13','$p14');");
        $out_param_query = $this->db->query('select @p0 AS `paymnt_id`');

        $result = $out_param_query->result();

        return true;
    }

    function update_status($itemids,$stat,$price){
        $query = $this->db->query("update sales_order_items set status = ?,price=? where item_id = ? ", array('status'=>$stat,'price'=>$price,'item_id'=>$itemids));
        return true;
    }

    function get_due_amnt($prty_nme){

        $query = $this->db->query("select due_amount from payment_wholesale where party_name=?",array('party_name'=>$prty_nme));
        $result = $query->result();
        return $result;
    }

    function login_cred($usr,$pass){

        $query = $this->db->query("select user_id,password,user_type from users where user_id=? && password=?",array('username'=>$usr,'pasword'=>$pass));
        $result = $query->result();
        return $result;
    }

    function check_name($prtyname){
        $query = $this->db->query("SELECT a.payment_wholesale_id,a.total_amount,a.paid_amount,b.due_amount,b.name,b.address FROM party_details b LEFT JOIN payment_wholesale a ON a.party_name=b.name where b.name=?",array('party_name'=>$prtyname));
        $result = $query->result();
        return $result;
    }

    function partychck($prtyname){

        $query = $this->db->query("SELECT * FROM party_details a where a.name='$prtyname'");
        $result = $query->result();
        return $result;
    }

    function update_prty_due_blnce($prtyname,$updatedblnce){
        $query = $this->db->query("update party_details set due_amount = ? where name = ? ", array('due_amnt'=>$updatedblnce,'prty_name'=>$prtyname));
        return true;
    }

    function insrtprtydtails($nme,$id,$dueamnt){
        $query = $this->db->query("INSERT INTO party_details (id,name,address,due_amount) VALUES (?,?,?,?)", array('id'=>$id,'prty_name'=>$nme,'prty_adress'=>"",'due_amnt'=>$dueamnt));
        return 0;
    }

    /**Added for product type and product name**/

    function saveprodtype($data){


        $p0=$data['prod_type_id'];
        $p1=$data['prod_type'];

        if($p0 == ''){
            $query = $this->db->query("INSERT INTO product_type(prod_type_id,prod_type_name) VALUES (?, ?)", array('prod_type_id'=>$p0,'prod_type'=>$p1));
            return true;
        }else{

            $query = $this->db->query("UPDATE product_type SET prod_type_name= ? WHERE prod_type_id= ?", array('prod_type'=>$p1,'prod_type_id'=>$p0));
            return true;
        }
    }

    function saveprodtname($data){

        $p0=$data['prod_name_id'];
        $p1=$data['prod_name'];
        $p2=$data['prod_type'];

        if($p0 == ''){
            $query = $this->db->query("INSERT INTO product_name(prod_name_id,prod_name,product_type) VALUES (?, ?, ?)", array('prod_name_id'=>$p0,'prod_name'=>$p1,'prod_type'=>$p2));
            return true;
        }else{

            $query = $this->db->query("UPDATE product_name SET prod_name= ? WHERE prod_name_id= ?", array('prod_name'=>$p1,'prod_name_id'=>$p0));
            return true;
        }
    }

    function get_prod_type(){

        $query = $this->db->query("select * from product_type");
        $result = $query->result();
        return $result;
    }

    function get_prod_name($type){

        $query = $this->db->query("select * from product_name where product_type=?", array('prod_type'=>$type));
        $result = $query->result();
        return $result;
    }

    function delprodtname($prod_id){

        $query = $this->db->query("delete from product_name where prod_name_id = ?" , array('prod_name_id'=>$prod_id));
        return true;
    }


    function delprodtype($prod_id){

        $query = $this->db->query("delete from product_type where prod_type_id = ?" , array('prod_type_id'=>$prod_id));
        return true;
    }

    function get_party_name(){

        $query = $this->db->query("select * from  party_details");
        $result = $query->result();
        return $result;
    }

    /*function for getting product name*/

    function get_product_name(){

        $query = $this->db->query("select * from  product_name");
        $result = $query->result();
        return $result;
    }

    function delpartyname($id){

        $query = $this->db->query("delete from party_details where id = ?" , array('party_id'=>$id));
        return true;
    }

    function savepartyname($data){

        $p0=$data['party_name_id'];
        $p1=$data['party_name'];
        $p2=$data['party_address'];
        $p3=$data['opening_bal'];
        $p4=$data['tin_no'];
        $p5=$data['cst_regd_no'];

        if($p0 == ''){
            $query = $this->db->query("INSERT INTO party_details(id,name,address,due_amount,tin,cst_regd_no) VALUES (?, ?, ?, ?, ?, ?)", array('party_id'=>$p0,'party_name'=>$p1,'address'=>$p2,'due_amount'=>$p3,'tin_no'=>$p4,'cst_regd_no'=>$p5));
            return true;
        }else{

            $query = $this->db->query("UPDATE party_details SET name= ?,address=?,due_amount=?,tin=?,cst_regd_no=? WHERE id= ?", array('party_name'=>$p1,'address'=>$p2,'due_amount'=>$p3,'tin_no'=>$p4,'cst_regd_no'=>$p5,'party_id'=>$p0));
            return true;
        }
    }

    function delete_item($data){
        $this->db->query("delete from `sales_order_items` WHERE item_id in($data)");
        return true;
    }

    /* Added By nandini for saving data in database*/

    function update_save_item($itemids,$stat){


//        $query = $this->db->query("UPDATE sales_order_items SET status = ? WHERE item_id=? ", array($stat,$itemids));
        $query = $this->db->query("UPDATE sales_order_items SET status = ? WHERE item_id in($itemids)", array('status'=>$stat));



//        print_r($stat);exit();
        return true;


    }



//    function update_prty_due_blnce($prtyname,$updatedblnce){
//        $query = $this->db->query("update party_details set due_amount = ? where name = ? ", array('due_amnt'=>$updatedblnce,'prty_name'=>$prtyname));
//        return true;
//    }



    function get_stock_list_spec($prod_name,$sph,$cyl,$axis,$addition){

        $query=$this->db->query("SELECT * FROM stock_master WHERE product_name LIKE '$prod_name%' and sph like '$sph%' and cyl like '$cyl%' and axis like '$axis%' and addition like '$addition%';");
        $result = $query->result();
        return $result;

    }

    function trancsaction_all($lim){

        $query=$this->db->query("SELECT *,a.due_amount as item_due_amnt FROM payment_wholesale a LEFT JOIN party_details b ON a.party_name = b.name order by a.updated_on desc limit $lim");
        $result = $query->result();
        return $result;
    }

    function select_item(){
        $result=$this->db->query("SELECT * FROM sales_order_items a LEFT JOIN sales_order b ON a.sales_order_id=b.id ;");
        return $result->result();
    }

    function itemdetails($itemid){

        $result=$this->db->query("SELECT * FROM `sales_order_items`  where item_id in($itemid)");
        return $result->result();

    }

    function update_status_outsource($itemids,$stat){
    $query = $this->db->query("update sales_order_items set status = ? where item_id in($itemids) ", array('status'=>$stat));
    return true;
}

    function update_status_purchageorderlist($itemids,$stat){
        $query = $this->db->query("update sales_order_items set status = ? where item_id=? ", array($stat,$itemids));

//        print_r($query);exit();
        return true;
    }

    function update_status_received_order_list($itemids,$stat){
        $query = $this->db->query("update sales_order_items set status = ? where item_id = ?", array($stat,$itemids));
        return true;
    }

    /*query added for search_list*/

    function getseachitemdetails($type)
    {

        $query = $this->db->query("SELECT * FROM sales_order a INNER JOIN sales_order_items b ON a.id = b.sales_order_id  where bill_no=?", array('bill_no' => $type));
//         print_r($query);exit();
        $result = $query->result();
        return $result;
    }


     function gettingbillno($bill_no){

         $query = $this->db->query("SELECT bill_no  FROM sales_order where bill_no=?", array('bill_no'=>$bill_no));

         $result = $query->result();
         return $result;
     }


    function getpartyname($party_name){

//        print_r("In Party name array");exit();



        $query = $this->db->query("SELECT party_name FROM sales_order where party_name=?",array('party_name'=>$party_name));
//        print_r($query);exit();


        $result = $query->result();
        return $result;
//
    }
    /*functionality added for the sale_report page*/


    function getseachitemsbydate($data)
    {
        $p0=$data['from_date'];
        $p1=$data['to_date'];

        $query = $this->db->query( "SELECT a.sales_order_id as sales_id, a.bill_no,a.order_date,a.party_name,b.* FROM sales_order a INNER JOIN sales_order_items b ON a.id = b.sales_order_id and (a.order_date >= ? AND a.order_date <= ?)",array('from_date'=>$p0,'to_date'=>$p1));

//        print_r($query);exit();

        $result = $query->result();
        return $result;

    }



    function getsalesreportbyall($data){
        $p0=$data['from_date'];
        $p1=$data['to_date'];
        $p2=$data['selectpaymentmode'];
        $p3=$data['party_name'];
        $p4=$data['selectproducttype'];
        $p5=$data['bill_no'];


//        print_r($)


//        $query = $this->db->query( "SELECT a.sales_order_id as sales_id, a.bill_no,a.order_date,a.party_name,b.* FROM sales_order a INNER JOIN sales_order_items b ON a.id = b.sales_order_id and (a.order_date >= ? AND a.order_date <= ?)",array('from_date'=>$p0,'to_date'=>$p1));
//        $query = $this->db->query( "SELECT * FROM payment_wholesale a INNER JOIN  sales_order_items b on a.payment_wholesale_id =b.payment_wholesale_id and(a.updated_on >= ? AND a.updated_on  <= ?)",array('from_date'=>$p0,'to_date'=>$p1,'selectpaymentmode'=>$p2,'party_name'=>'$p3','selectproducttype'=>'$p4','$p5'=>'bill_no'));
        $query = $this->db->query( "SELECT * FROM eyedcordb.payment_wholesale a inner join eyedcordb.sales_order_items b on a.payment_wholesale_id=b.payment_wholesale_id where  (a.updated_on BETWEEN ? AND ?) AND (a.party_name=? || a.bill_no=? || b.product_type= ? || a.payment_type= ?) order by a.updated_on desc;",array('from_date'=>$p0,'to_date'=>$p1,'selectpaymentmode'=>$p2,'party_name'=>$p3,'selectproducttype'=>$p4,'bill_no'=>$p5));

//        print_r($query);exit();

        $result = $query->result();
        return $result;

    }


    function get_print_data($typ){

        $query = $this->db->query("SELECT * FROM sales_order a INNER JOIN sales_order_items b ON a.id = b.sales_order_id  where bill_no=? and b.status=?", array('bill_no' => $typ,'status'=>'Not Ordered'));

//        print_r($query);exit();
        $result = $query->result();

        return $result;
    }

    //function to convert number into words
    function convert_number_to_words($number) {

        print_r($number);exit();

        $hyphen      = '-';
        $conjunction = ' and ';
        $separator   = ', ';
        $negative    = 'negative ';
        $decimal     = ' point ';
        $dictionary  = array(
            0                   => 'zero',
            1                   => 'one',
            2                   => 'two',
            3                   => 'three',
            4                   => 'four',
            5                   => 'five',
            6                   => 'six',
            7                   => 'seven',
            8                   => 'eight',
            9                   => 'nine',
            10                  => 'ten',
            11                  => 'eleven',
            12                  => 'twelve',
            13                  => 'thirteen',
            14                  => 'fourteen',
            15                  => 'fifteen',
            16                  => 'sixteen',
            17                  => 'seventeen',
            18                  => 'eighteen',
            19                  => 'nineteen',
            20                  => 'twenty',
            30                  => 'thirty',
            40                  => 'fourty',
            50                  => 'fifty',
            60                  => 'sixty',
            70                  => 'seventy',
            80                  => 'eighty',
            90                  => 'ninety',
            100                 => 'hundred',
            1000                => 'thousand',
            1000000             => 'million',
            1000000000          => 'billion',
            1000000000000       => 'trillion',
            1000000000000000    => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens   = ((int) ($number / 10)) * 10;
                $units  = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds  = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= convert_number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    }

}
