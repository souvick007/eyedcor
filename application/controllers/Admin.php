<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{

    function __construct(){

        parent::__construct(); // needed when adding a constructor to a controller
        $this->load->model('Postmodel');
    }

    /*functionality added for sales report page */

    function sales_report(){
        if($this->input->post('report')) {

            $from_date = $this->input->post('from_date');
            $to_date = $this->input->post('to_date');
            $selectpaymentmode = $this->input->post('selectpaymentmode');
            $party_name = $this->input->post('party_name');
            $selectproducttype = $this->input->post('selectproducttype');
            $bill_no = $this->input->post('bill_no');

            $data['from_date'] = $from_date;
            $data['to_date'] = $to_date;
            $data['selectpaymentmode'] = $selectpaymentmode;
            $data['party_name'] = $party_name;
            $data['selectproducttype'] = $selectproducttype;
            $data['bill_no'] = $bill_no;


            $datalist = array(
                'from_date' => $from_date,
                'to_date' => $to_date,
                'selectpaymentmode' => $selectpaymentmode,
                'party_name' => $party_name,
                'selectproducttype' => $selectproducttype,
                'bill_no' => $bill_no,
            );

//            print_r($datalist);exit();

            $data['getsalesorderitemdetail'] = $this->Postmodel->getsalesreportbyall($datalist);
            @$this->db->free_db_resource();

//         print_r($data['getsalesorderitemdetail']);exit();

        }else{
            $from_date = date("Y-m-d",strtotime("-1 days"));
            $to_date= date ("Y-m-d");

            $selectpaymentmode = $this->input->post('selectpaymentmode');
            $party_name = $this->input->post('party_name');
            $selectproducttype = $this->input->post('selectproducttype');
            $bill_no = $this->input->post('bill_no');


            $data['from_date']= $from_date;
            $data['to_date']= $to_date;
            $data['selectpaymentmode']=$selectpaymentmode ;
            $data['party_name']= $party_name;
            $data['selectproducttype']= $selectproducttype;
            $data['bill_no']= $bill_no;
//
            $datalist=array(
                'from_date' =>  $from_date,
                'to_date' => $to_date,
                'selectpaymentmode'=>$selectpaymentmode,
                'party_name'=>$party_name,
                'selectproducttype'=>$selectproducttype,
                'bill_no'=>$bill_no,
            );

            $data['getsalesorderitemdetail']= $this->Postmodel->getsalesreportbyall($datalist);
            @$this->db->free_db_resource();

//         print_r($data['getsalesorderitemdetail']);exit();

        }
        $this->load->view('app/admin/sales_report',$data);
    }
/* functionality added for party name autopopulate*/


    function checkpartynamesales_report(){

        $party_name=$this->input->post('party_name');

        $data['getpartyname']= $this->Postmodel->getpartyname($party_name);
        @$this->db->free_db_resource();
//        print_r($data['getpartyname']);exit();


        $count=count($data['getpartyname']);

        echo $count;

    }






    function product_type(){

        $data['producttype'] = $this->Postmodel->get_prod_type();
        @$this->db->free_db_resource();

//        print_r( $data['producttype']);exit();

        $this->load->view('app/admin/producttype',$data);
    }

    function product_type_save(){

        $datalist=array(
            'prod_type_id'=>$this->input->post('product_type_id'),
            'prod_type'=>strtoupper($this->input->post('product_type')),
        );

//        print_r($datalist);exit();

        $data['saveproducttype']=$this->Postmodel->saveprodtype($datalist);
        @$this->db->free_db_resource();

        redirect($this->agent->referrer());
    }

/*Product Name For Ready/Rx */
    /*Start*/
    function product_name(){

        $type='ready';
        $typeinuprcse=strtoupper($type);

        $data['productname'] = $this->Postmodel->get_prod_name($typeinuprcse);
        @$this->db->free_db_resource();

        $this->load->view('app/admin/productname',$data);
    }

    function product_name_save(){

        $type='ready';
        $datalist=array(
            'prod_name_id'=>$this->input->post('product_name_id'),
            'prod_name'=>strtoupper($this->input->post('product_name')),
            'prod_type'=>strtoupper($type),
        );

        $data['saveproductname']=$this->Postmodel->saveprodtname($datalist);
        @$this->db->free_db_resource();

        redirect($this->agent->referrer());
    }

    function del_prod_name(){
        $data['delprodtname']=$this->Postmodel->delprodtname($this->uri->segment(3));
        @$this->db->free_db_resource();
        redirect($this->agent->referrer());
    }
    /*End*/

    /*Product Name For Contact Lens */
    /*Start*/
    function product_name_cont(){

        $type='contact_lens';
        $typeinuprcse=strtoupper($type);
        $data['productname'] = $this->Postmodel->get_prod_name($typeinuprcse);
        @$this->db->free_db_resource();

        $this->load->view('app/admin/productnamecont',$data);
    }

    function product_name_save_cont(){

        $type='contact_lens';
        $datalist=array(
            'prod_name_id'=>$this->input->post('product_name_id'),
            'prod_name'=>$str = strtoupper($this->input->post('product_name')),
            'prod_type'=>strtoupper($type),
        );

        $data['saveproductname']=$this->Postmodel->saveprodtname($datalist);
        @$this->db->free_db_resource();

        redirect($this->agent->referrer());
    }

    function del_prod_name_cont(){
        $data['delprodtname']=$this->Postmodel->delprodtname($this->uri->segment(3));
        @$this->db->free_db_resource();
        redirect($this->agent->referrer());
    }
    /*End*/

    /*Product Name For Grinding */
    /*Start*/
    function product_name_grind(){

        $type='grinding';
        $typeinuprcse=strtoupper($type);
        $data['productname'] = $this->Postmodel->get_prod_name($typeinuprcse);
        @$this->db->free_db_resource();

        $this->load->view('app/admin/productnamegrind',$data);
    }

    function product_name_save_grind(){

        $type='grinding';
        $datalist=array(
            'prod_name_id'=>$this->input->post('product_name_id'),
            'prod_name'=>$str = strtoupper($this->input->post('product_name')),
            'prod_type'=>strtoupper($type),
        );

        $data['saveproductname']=$this->Postmodel->saveprodtname($datalist);
        @$this->db->free_db_resource();

        redirect($this->agent->referrer());
    }

    function del_prod_name_grind(){
        $data['delprodtname']=$this->Postmodel->delprodtname($this->uri->segment(3));
        @$this->db->free_db_resource();
        redirect($this->agent->referrer());
    }
    /*End*/

    /*Product Name For Solution */
    /*Start*/
    function product_name_soln(){

        $type='Solution';
        $typeinuprcse=strtoupper($type);
        $data['productname'] = $this->Postmodel->get_prod_name($typeinuprcse);
        @$this->db->free_db_resource();

        $this->load->view('app/admin/productnamesoln',$data);
    }

    function product_name_save_soln(){

        $type='Solution';
        $datalist=array(
            'prod_name_id'=>$this->input->post('product_name_id'),
            'prod_name'=>$str = strtoupper($this->input->post('product_name')),
            'prod_type'=>strtoupper($type),
        );

        $data['saveproductname']=$this->Postmodel->saveprodtname($datalist);
        @$this->db->free_db_resource();

        redirect($this->agent->referrer());
    }

    function del_prod_name_soln(){
        $data['delprodtname']=$this->Postmodel->delprodtname($this->uri->segment(3));
        @$this->db->free_db_resource();
        redirect($this->agent->referrer());
    }
    /*End*/

    /*Product Name For Accessories */
    /*Start*/
    function product_name_acc(){

        $type='Accessory';
        $typeinuprcse=strtoupper($type);
        $data['productname'] = $this->Postmodel->get_prod_name($typeinuprcse);
        @$this->db->free_db_resource();

        $this->load->view('app/admin/productnameacc',$data);
    }

    function product_name_save_acc(){

        $type='Accessory';
        $datalist=array(
            'prod_name_id'=>$this->input->post('product_name_id'),
            'prod_name'=>$str = strtoupper($this->input->post('product_name')),
            'prod_type'=>strtoupper($type),
        );

        $data['saveproductname']=$this->Postmodel->saveprodtname($datalist);
        @$this->db->free_db_resource();

        redirect($this->agent->referrer());
    }

    function del_prod_name_acc(){
        $data['delprodtname']=$this->Postmodel->delprodtname($this->uri->segment(3));
        @$this->db->free_db_resource();
        redirect($this->agent->referrer());
    }
    /*End*/

    function del_prod_type(){
//        print_r($this->uri->segment(3));exit();
        $data['delprodtype']=$this->Postmodel->delprodtype($this->uri->segment(3));
        @$this->db->free_db_resource();

        redirect($this->agent->referrer());
    }

    function party_name(){

        $data['partyname'] = $this->Postmodel->get_party_name();
        @$this->db->free_db_resource();

//        print_r($data['partyname']);exit();

        $this->load->view('app/admin/partyname',$data);
    }

    function del_party_name(){

        $data['delprodtype']=$this->Postmodel->delpartyname($this->uri->segment(3));
        @$this->db->free_db_resource();

        redirect($this->agent->referrer());

    }

    function party_name_save(){

        $datalist=array(
            'party_name_id'=>$this->input->post('party_name_id'),
            'party_name'=>strtoupper($this->input->post('party_name')),
            'party_address'=>$this->input->post('party_address'),
            'opening_bal'=>$this->input->post('opening_bal'),
            'tin_no'=>$this->input->post('tin_no'),
            'cst_regd_no'=>$this->input->post('cst_regd_no'),
        );

//        print_r($datalist);exit();

        $data['saveproductname']=$this->Postmodel->savepartyname($datalist);
        @$this->db->free_db_resource();

        redirect($this->agent->referrer());
    }

    function deliv_list_manage(){

        $datalist=array(
            'sales_order_id'=>0,
            'order_status' =>'Delivered',
            'offset'=>0,
            'limit'=>100,
        );

        $data['delivlist']=$this->Postmodel->get_sales_order_item($datalist);
        @$this->db->free_db_resource();

        $this->load->view('app/admin/deliv_list',$data);
    }

    function del_deliv_data(){

//       $del=$this->input->post('delete');
//        $save=$this->input->post('save');

        $selectcheckbox=$this->input->post('select_check');
        $status=$this->input->post('selectstatus');

//        print_r($this->input->post('select_check'));exit();

        if($this->input->post('delete') != null){

//        if($del!==""){

            if($selectcheckbox == ""){

                $this->session->set_flashdata('messageError', 'Please choose data for delete');
                redirect($this->agent->referrer());
            }else{

                $exp = implode(',', $selectcheckbox);

//            print_r($exp);exit();

                $data['deldata']=$this->Postmodel->delete_item($exp);

//
            $this->session->set_flashdata('messageSuccess', 'Choosen Data Deleted');
            redirect($this->agent->referrer());
            }

        }


/*Added for the save functionality*/

        if($this->input->post('save') != null){
//         if($save!=="")  {

             if($selectcheckbox == ""){

                 $this->session->set_flashdata('messageError', 'Please select data for save');
                 redirect($this->agent->referrer());
             }
             else{

//print_r($selectcheckbox);exit();

                 for($i=0;$i<count($selectcheckbox);$i++){

                     $explodeval=explode(',',$selectcheckbox[$i]);

                     $data['savedata']=$this->Postmodel->update_save_item($explodeval[0],$status);

//                     print_r($explodeval[0]);exit();
//                     print_r($data['savedata']);exit();

                     if($explodeval[1] == 'READY' && $status == 'Return'){

                         $data=array(
                             'prod_name'=>$explodeval[2],
                             'sph'=>$explodeval[3],
                             'cyl'=>$explodeval[4],
                             'axis'=>$explodeval[5],
                             'addition'=>$explodeval[6],
                             'quant'=>$explodeval[7],
                         );

                         $data['addtostock']=$this->Postmodel->add_to_stock($data);

                     }
                 }

//                 $data['savedata']=$this->Postmodel->update_save_item($explodeval[0],$status);

//                 print_r($data['savedata']);exit();

                 $this->session->set_flashdata('messageSuccess', 'Choosen Data has been Saved');
                 redirect($this->agent->referrer());
             }
         }
    }

    /* Added By Nandini for saving data in the delievery list page*/














    function get_search_item(){

//        print_r($this->input->post('bill_no'));exit();
        $data['getsearchitem']=$this->Postmodel->getseachitemdetails($this->input->post('bill_no'));
        @$this->db->free_db_resource();
//        print_r($data['getsearchitem']);exit();


//        redirect($this->agent->referrer());

    }

    /*newly added function for search  records according to date*/

//    function godate(){
////        print_r("HII");exit();
//
//    }

    function search_list(){

     if($this->input->post('go')){

         $from_date = $this->input->post('from_date');
         $to_date= $this->input->post('to_date');

         $data['from_date']= $from_date;
         $data['to_date']= $to_date;


         $datalist=array(
             'from_date' =>  $from_date,
             'to_date' => $to_date,
         );

         $data['getsalesorderitemdetail']= $this->Postmodel->getseachitemsbydate($datalist);
         @$this->db->free_db_resource();

//         print_r($data['getsalesorderitemdetail']);exit();

     }else{
         $from_date = date("Y-m-d",strtotime("-1 days"));
         $to_date= date ("Y-m-d");

         $data['from_date']= $from_date;
         $data['to_date']= $to_date;

         $datalist=array(
             'from_date' =>  $from_date,
             'to_date' => $to_date,
         );

         $data['getsalesorderitemdetail']= $this->Postmodel->getseachitemsbydate($datalist);
         @$this->db->free_db_resource();

//         print_r($data['getsalesorderitemdetail']);exit();

     }
        $this->load->view('app/admin/search_list',$data);
    }

}