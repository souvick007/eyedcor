<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wholesale extends CI_Controller
{

    function __construct(){

        parent::__construct(); // needed when adding a constructor to a controller
        $this->load->model('Postmodel');

        if(!$this->session->userdata('logged_in')){
            redirect('welcome');
        }
//        print_r($this->session->userdata());exit();
    }

    function bulkpurchase(){

        $datalist=array(
            'sales_order_id'=>0,
            'order_status' =>'Bulk Purchase',
            'offset'=>0,
            'limit'=>1000,
        );

        $data['bulkpurchaselist']=$this->Postmodel->get_sales_order_item_of_bulk_purchase($datalist);
        @$this->db->free_db_resource();



        $this->load->view('app/wholesale/bulk_purchase',$data);

//                print_r($data['bulkpurchaselist']);exit();
    }



    //function for  fetching party name
    public function get_party_name(){

        $partyName = array();

        $data['party_name']=$this->Postmodel->get_party_name();
        @$this->db->free_db_resource();

        for($i=0;$i<count($data['party_name']);$i++){

            array_push($partyName,$data['party_name'][$i]->name);

//            print_r($partyName);exit();
        }

        $party_name_auto=json_encode($partyName);

//        print_r($party_name_auto);exit();

//        $data["party_name_auto"]=$party_name_auto;
        return $party_name_auto;
    }


    public function get_product_name(){



        $productName = array();

        $data['product_name'] = $this->Postmodel->get_product_name();
        @$this->db->free_db_resource();

        for($i=0;$i<count($data['product_name']);$i++){

            array_push($productName,$data['product_name'][$i]->prod_name);
        }


        $product_name = json_encode($productName);
//        $data["product_name_auto"]=$product_name;

        return $product_name;

    }

    function purchase(){

        $data['getdet']=$this->Postmodel->get_country_list();

        $this->load->view('app/wholesale/purchase');
    }

    function sale(){

        $data['cont_spec'] = $this->config->item('cont_spec');

        $cont_spec=json_encode($data['cont_spec']);

//        print_r($this->get_party_name());exit();

        $data["party_name"]=$this->get_party_name();
//        $data["party_name_hidd"]=$this->get_party_name();
        $data["cont_spec"]=$cont_spec;

        $this->load->view('app/wholesale/sale',$data);
    }

    function welltest(){
        print_r($this->input->post('data'));exit();

    }

    function sale_save(){

        $datalist=array(
            'order_id'=>$this->input->post('order_id'),
            'bill_no'=>$this->input->post('bill_no'),
            'date'=>$this->input->post('date'),
            'party_name'=>$this->input->post('party_name'),
            'order_status'=>'Not Ordered',
        );

//        print_r($datalist);exit();


        $data['savesales'] = $this->Postmodel->save_update_sales_order($datalist);
        @$this->db->free_db_resource();

//        print_r($data['savesales'] );exit();

        $var=$data['savesales'][0]->order_id;
        $json=$this->input->post('hidden_val_json');
        $obj=json_decode($json);

        print_r($obj);
        print_r('<br>');

        for($i=0;$i<sizeof($obj);$i++){

            $datalist1=array(

                'item_id'=>0,
                'order_id'=>$var,
                'prod_type'=>$obj[$i]->prod_type,
                'prod_name'=>$obj[$i]->prod_name,
                'quant'=>$obj[$i]->quantity,
                'comp_name'=>$obj[$i]->comp_name,
                'add_type'=>$obj[$i]->add_type,
                'specification'=>$obj[$i]->specification,
                'sph'=>$obj[$i]->sph,
                'cyl'=>$obj[$i]->cyl,
                'axis'=>$obj[$i]->axis,
                'addition'=>$obj[$i]->addition,
                'dia'=>$obj[$i]->dia,
                'base'=>$obj[$i]->base,
                'side'=>$obj[$i]->side,
                'remarks'=>$obj[$i]->remarks,
                'order_status'=>'Not Ordered',
                'frame'=>$obj[$i]->frame,
                'lens'=>$obj[$i]->lens,
                'supplier_name'=>$obj[$i]->supplier_name,
                'item_price'=>0,
                'item_ref_no'=>$obj[$i]->item_ref_no
            );

//            print_r($datalist1);exit();

            $data['savesalesitem'] = $this->Postmodel->save_update_sales_order_item($datalist1);
        }
        $this->session->set_flashdata('messageSuccess', 'Data Saved Successfully');


        redirect('wholesale/sale');
    }

    function orderlist(){

        $data['cont_spec'] = $this->config->item('cont_spec');


        $data['party_name_auto']=$this->get_party_name();


        $data['product_name_auto']=$this->get_product_name();




//        $productName = array();
//
//        $data['product_name'] = $this->Postmodel->get_product_name();
//        @$this->db->free_db_resource();
//
//        for($i=0;$i<count($data['product_name']);$i++){
//
//            array_push($productName,$data['product_name'][$i]->prod_name);
//        }
//
//
//        $product_name = json_encode($productName);
//        $data["product_name_auto"]=$product_name;


        if($this->uri->segment(3) != ""){


//            $data["cont_spec"]=$cont_spec;

//            print_r($data['party_name']);exit();

            $datalist = array(
                'sales_order_id' => $this->uri->segment(3),
                'order_status' =>'Not Ordered',
                'offset' => 0,
                'limit' => 100,
            );

            $data['orderlistdata']                                                                                                                 = $this->Postmodel->get_sales_order_item($datalist);
            @$this->db->free_db_resource();

//            print_r($data['orderlistdata']);exit();


            $phpdate=$data['orderlistdata'][0]->order_date;
            $mysqldate = date('Y-m-d', strtotime( $phpdate ));
            $data['date']=$mysqldate;
            $data['id']=$data['orderlistdata'][0]->id;
            $data['sales_order_id']=$data['orderlistdata'][0]->sales_order_id;
            $data['bill_no']=$data['orderlistdata'][0]->bill_no;
            $data['party_name']=$data['orderlistdata'][0]->party_name;
            $data['item_id']=$data['orderlistdata'][0]->item_id;
            $data['product_type']=$data['orderlistdata'][0]->product_type;
            $data['product_name']=$data['orderlistdata'][0]->product_name;
            $data['company_name']=$data['orderlistdata'][0]->company_name;
            $data['specification']=$data['orderlistdata'][0]->specification;
            $data['sph']=$data['orderlistdata'][0]->sph;
            $data['cyl']=$data['orderlistdata'][0]->cyl;
            $data['axis']=$data['orderlistdata'][0]->axis;
            $data['addition']=$data['orderlistdata'][0]->addition;
            $data['diameter']=$data['orderlistdata'][0]->diameter;
            $data['base']=$data['orderlistdata'][0]->base;
            $data['side']=$data['orderlistdata'][0]->side;
            $data['quantity']=$data['orderlistdata'][0]->quantity;
            $data['remarks']=$data['orderlistdata'][0]->remarks;
            $data['status']=$data['orderlistdata'][0]->status;
            $data['frame']=$data['orderlistdata'][0]->frame;
            $data['lens']=$data['orderlistdata'][0]->lens;
            $data['price']=$data['orderlistdata'][0]->price;
            $data['supplier_name']=$data['orderlistdata'][0]->supplier_name;
            $data['add_type']=$data['orderlistdata'][0]->add_type;
            $data['ref_no']=$data['orderlistdata'][0]->ref_no;
        }

        $datalist=array(
            'sales_order_id'=>0,
            'order_status' =>'Not Ordered',
            'offset'=>0,
            'limit'=>1000,
        );

        $data['orderlistdata'] = $this->Postmodel->get_sales_order_item1($datalist);
//        @$this->db->free_db_resource();

        $this->load->view('app/wholesale/orderlist',$data);
    }

    function orderlist_save(){

        //Checking order number
        if($this->input->post('order_no') != ""){
            $ord_id=$this->input->post('order_no');
        }else{
            redirect('wholesale/orderlist');
        }

        $datalist=array(
            'order_id'=>$ord_id,
            'bill_no'=>$this->input->post('bill_no'),
            'date'=>$this->input->post('date'),
            'party_name'=>$this->input->post('party_name'),
            'order_status'=>$this->input->post('order_status'),
        );

        $data['orderlistupdate'] = $this->Postmodel->save_update_sales_order($datalist);
        $ord_id=$data['orderlistupdate'][0]->order_id;
        @$this->db->free_db_resource();

        $datalist1=array(
            'item_id'=>$this->input->post('item_id'),
            'order_id'=>$ord_id,
            'prod_type'=>$this->input->post('prod_type'),
            'prod_name'=>$this->input->post('prod_name'),
            'quant'=>$this->input->post('quantity'),
            'comp_name'=>$this->input->post('comp_name'),
            'add_type'=>$this->input->post('add_type'),
            'specification'=>$this->input->post('specification'),
            'sph'=>$this->input->post('sph'),
            'cyl'=>$this->input->post('cyl'),
            'axis'=>$this->input->post('axis'),
            'addition'=>$this->input->post('addition'),
            'dia'=>$this->input->post('dia'),
            'base'=>$this->input->post('base'),
            'side'=>$this->input->post('side'),
            'remarks'=>$this->input->post('remarks_dup'),
            'order_status'=>$this->input->post('order_status'),
            'frame'=>$this->input->post('frame_dup'),
            'lens'=>$this->input->post('lens_dup'),
            'supplier_name'=>$this->input->post('supplier_name'),
            'item_price'=>$this->input->post('item_price'),
            'item_ref_no'=>$this->input->post('ref_no'),
        );

//        print_r($datalist1);exit();

        $data['saveorderlistitem'] = $this->Postmodel->save_update_sales_order_item($datalist1);
        @$this->db->free_db_resource();

        if($this->input->post('order_status') == 'Delivered'){

            $datalist=array(
                'paymnt_wholesale_id'=>0,
                'inv_no'=>$this->input->post('bill_no'),
                'ord_no'=>$ord_id,
                'bill_no'=>$this->input->post('bill_no'),
                'item_id'=>$this->input->post('item_id'),
                'prty_name'=>$this->input->post('party_name'),
                'amount'=>$this->input->post('item_price'),
                'discnt_amount'=>0,
                'ser_tax'=>0,
                'tot_amnt'=>$this->input->post('item_price'),
                'payed'=>$this->input->post('cash_amnt'),
                'due_amount'=>0,
                'updated_by'=>1,
                'indvprice'=>$this->input->post('item_price'),
                'payment_type'=>'Cash',
                'payment_type_id'=>'',
            );

            $data['savepayment']=$this->Postmodel->save_payment($datalist);
            @$this->db->free_db_resource();

        }

        if($this->input->post('prod_type') == "READY" && $this->input->post('order_status') == "Received"){

            $data['stockadd'] = $this->Postmodel->add_to_stock($datalist1);
        }
        if($this->input->post('prod_type') == "READY" && $this->input->post('order_status') == "Delivered To Counter"){

            $data['stockremove'] = $this->Postmodel->remove_from_stock($datalist1);
        }

        //Saving For pending quantiy

        if($this->input->post('pending_quantity') > 0){
            $datalist2=array(
                'item_id'=>0,
                'order_id'=>$ord_id,
                'prod_type'=>$this->input->post('prod_type'),
                'prod_name'=>$this->input->post('prod_name'),
                'quant'=>$this->input->post('pending_quantity'),
                'comp_name'=>$this->input->post('comp_name'),
                'add_type'=>$this->input->post('add_type'),
                'specification'=>$this->input->post('specification'),
                'sph'=>$this->input->post('sph'),
                'cyl'=>$this->input->post('cyl'),
                'axis'=>$this->input->post('axis'),
                'addition'=>$this->input->post('addition'),
                'dia'=>$this->input->post('dia'),
                'base'=>$this->input->post('base'),
                'side'=>$this->input->post('side'),
                'remarks'=>$this->input->post('remarks_dup'),
                'order_status'=>'Not Ordered',
                'frame'=>$this->input->post('frame_dup'),
                'lens'=>$this->input->post('lens_dup'),
                'supplier_name'=>$this->input->post('supplier_name'),
                'item_price'=>0,
                'item_ref_no'=>'',
                'deliv_type'=>'',
            );

            $data['saveorderlistitem'] = $this->Postmodel->save_update_sales_order_item($datalist2);
            @$this->db->free_db_resource();
        }


        $this->session->set_flashdata('messageSuccess', 'Data Updated Successfully');

        redirect('wholesale/orderlist');

    }


    //AJAX Call to get specific data in orderlist
    function orderlistindvdata(){

        $datalist=array(
            'sales_order_id'=>$this->input->post('itemid'),
            'order_status' =>'Not Ordered',
            'offset'=>0,
            'limit'=>10,
        );

        $data['orderlistdata'] = $this->Postmodel->get_sales_order_item1($datalist);
        @$this->db->free_db_resource();

//        print_r($data['orderlistdata']);exit();

        $date=date_create($data['orderlistdata'][0]->order_date);
        $data['orderlistdata'][0]->order_date=date_format($date,"Y-m-d");

        $json=json_encode($data['orderlistdata']);

        echo $json;

    }

    function demo(){

        $data['msg'] = $this->config->item('test');

        $json=json_encode($data['msg']);

        $data["testing_data"]=$json;

//        print_r($json);exit;

        $this->load->view('app/demo1',$data);
    }

    function demo_save(){
//        print_r($_POST['data']);exit();
        $fullname=$this->input->post('fullname');
        $abc=$this->input->post('abc');
        $datalist=array(
            'fullname'=>$fullname,
            'abc'=>$abc,
        );
        array_push($data,$datalist);
        $ayz[]=$data;

        print_r($ayz);exit();
    }


    function test_data(){
//        print_r($this->input->post('tags'));exit();

        $json=$this->input->post('Welltest');
        $obj=json_decode($json);
        print_r($obj);exit();
    }

    function stocklist(){

        $data['stocklist'] = $this->Postmodel->get_stock_list();
//        print_r($data['stocklist']);exit();
        @$this->db->free_db_resource();

        $this->load->view('app/wholesale/stocklist',$data);
    }


    function outsource_list(){

        $datalist=array(
            'sales_order_id'=>0,
            'order_status' =>'Out Source',
            'offset'=>0,
            'limit'=>1000,
        );

        $data['outsourcelist']=$this->Postmodel->get_sales_order_item($datalist);
//
//        print_r($data['outsourcelist']);exit();

        $this->load->view('app/wholesale/out_source',$data);
    }

    function outsource_update(){



        $data['outsourceupdate'] = $this->Postmodel->update_stat($_GET['item_id'],$_GET['stat']);
        @$this->db->free_db_resource();

        redirect('wholesale/outsource_list');
    }

    function purchase_orderlist(){

        $datalist=array(
            'sales_order_id'=>0,
            'order_status' =>'Ordered',
            'offset'=>0,
            'limit'=>1000,
        );

        $data['orderedlist']=$this->Postmodel->get_sales_order_item($datalist);

        $this->load->view('app/wholesale/purchase_orderlist',$data);
    }

    function purchase_update(){

        $prod_name=str_replace('%20', '', $_GET['prod_name']);
        $sph=str_replace('%20', '', $_GET['sph']);
        $cyl=str_replace('%20', '', $_GET['cyl']);
        $axis=str_replace('%20', '', $_GET['axis']);
        $addition=str_replace('%20', '', $_GET['addition']);

        $data['orderlistupdate'] = $this->Postmodel->update_stat($_GET['id'],$_GET['stat']);
        @$this->db->free_db_resource();

        if($_GET['type'] == "READY" && $_GET['stat']== "Received"){

            $datalist2=array(
                'prod_name'=>$prod_name,
                'sph'=>$sph,
                'cyl'=>$cyl,
                'axis'=>$axis,
                'addition'=>$addition,
                'quant'=>$_GET['quant'],
            );

//            print_r($datalist2);exit();

            $data['returnstock'] = $this->Postmodel->add_to_stock($datalist2);
        }
        redirect('wholesale/purchase_orderlist');
    }


    function receivedlist(){

        $datalist=array(
            'sales_order_id'=>0,
            'order_status' =>'Received',
            'offset'=>0,
            'limit'=>1000,
        );

        $data['orderedlist']=$this->Postmodel->get_sales_order_item($datalist);
        @$this->db->free_db_resource();

//        print_r( $data['orderedlist']);exit();

        $this->load->view('app/wholesale/received_orderlist',$data);
    }

    function received_update(){

        $prod_name=str_replace('%20', '', $_GET['prod_name']);
        $sph=str_replace('%20', '', $_GET['sph']);
        $cyl=str_replace('%20', '', $_GET['cyl']);
        $axis=str_replace('%20', '', $_GET['axis']);
        $addition=str_replace('%20', '', $_GET['addition']);

        $data['orderlistupdate'] = $this->Postmodel->update_stat($_GET['id'],$_GET['stat'],$_GET['manf']);
        @$this->db->free_db_resource();

        if($_GET['type'] == "READY" && $_GET['stat']== "Delivered To Counter"){

            $datalist2=array(
                'prod_name'=>$prod_name,
                'sph'=>$sph,
                'cyl'=>$cyl,
                'axis'=>$axis,
                'addition'=>$addition,
                'quant'=>$_GET['quant'],
            );

            $data['returnstock'] = $this->Postmodel->remove_from_stock($datalist2);
        }

        redirect('wholesale/receivedlist');
    }

    function deliverylist(){

        $partyName=array();

        $data['party_name']=$this->Postmodel->get_party_name();

        for($i=0;$i<count($data['party_name']);$i++){

            array_push($partyName,$data['party_name'][$i]->name);
        }

        $data['party_name']=json_encode($partyName);

        $datalist=array(
            'sales_order_id'=>0,
            'order_status' =>'Delivered To Counter',
//            'order_status' =>'Bulk Purchase',
            'offset'=>0,
            'limit'=>1000,
        );

        $data['orderedlist']=$this->Postmodel->get_sales_order_item($datalist);
        @$this->db->free_db_resource();

//        print_r( $data['orderedlist']);exit();

        $this->load->view('app/wholesale/delivery_list',$data);
    }

    function delivery_update(){

        $prod_name=str_replace('%20', '', $_GET['prod_name']);
        $sph=str_replace('%20', '', $_GET['sph']);
        $cyl=str_replace('%20', '', $_GET['cyl']);
        $axis=str_replace('%20', '', $_GET['axis']);
        $addition=str_replace('%20', '', $_GET['addition']);

        $data['orderlistupdate'] = $this->Postmodel->update_stat($_GET['id'],$_GET['stat']);
        @$this->db->free_db_resource();



        redirect('wholesale/deliverylist');
    }

    function deliv_save(){

        $inv_no=$this->input->post('inv_no');
        $abc=json_encode($inv_no);

        print_r($abc);exit();
    }


    function amount_test(){

        $amount=$this->input->post('amount');

        $data['amount']=$amount;

        $this->load->views('ajaxcontent/test_demo',$data);

//        print_r($data['amount']);exit();
    }

    function  delivery_history(){

        $datalist=array(
            'sales_order_id'=>0,
            'order_status' =>'Delivered',
            'offset'=>0,
            'limit'=>1000,
        );

        $data['delivlist']=$this->Postmodel->get_sales_order_item($datalist);
        @$this->db->free_db_resource();

//    print_r($data['delivlist']);exit();

        $this->load->view('app/wholesale/delivery_history',$data);
    }

    function payment(){

        $data['inv_no']=$this->input->post('invoice_no');

        //checking value of checkbox
        if($this->input->post('payment')){


            $prty_nm=$this->input->post('party_name_prov');

            $data['checkprtyname'] = $this->Postmodel->partychck(strtoupper($prty_nm));
            @$this->db->free_db_resource();

//            print_r($data['checkprtyname']);exit();


            if($prty_nm == "" || $data['checkprtyname'] == ""){

                $this->session->set_flashdata('messageError', 'please provide valid party name');
                redirect($this->agent->referrer());
            }else{


                $data['due_amt']=$data['checkprtyname'][0]->due_amount;

                $this->load->view('app/wholesale/onlypayment',$data);
            }
        }else{

            $checkboxval=$this->input->post('options');

            $exp = implode(',', $checkboxval);

            $data['itemdetails'] = $this->Postmodel->get_sales_order_item_payment($exp);
            @$this->db->free_db_resource();

            $data['checkprtyname'] = $this->Postmodel->check_name(strtoupper($data['itemdetails'][0]->party_name));
            @$this->db->free_db_resource();

            if(sizeof($data['checkprtyname']) <= 0){
                $dueamnt = $this->Postmodel->insrtprtydtails(strtoupper($data['itemdetails'][0]->party_name),0,0);
                $data['due_amt']=$dueamnt;
            }else{
                $data['due_amt']=$data['checkprtyname'][0]->due_amount;
            }

            $data['item_ids']=$exp;
            $data['address']=$data['checkprtyname'][0]->address;


            $this->load->view('app/wholesale/payment',$data);
        }

    }

    //ajax call
    function payment_update(){

        $inv_no=$this->input->post('inv_no');
        $ord_no=$this->input->post('ord_no');
        $bill_no=$this->input->post('bill_no');
        $itemids=$this->input->post('item_id');
        $prtyname=$this->input->post('prty_name');
        $amount=$this->input->post('amount');
        $finalamnt=$this->input->post('finalamnt');
        $ser_tax=$this->input->post('serv_tax');
        $payed=$this->input->post('payed_amnt');
        $due_amount=$this->input->post('due_amount');
        $tot_amnt=$this->input->post('tot_amnt');
        $indvprice=$this->input->post('indvprice');
        $pay_type=$this->input->post('paymode');
        $pay_type_id=$this->input->post('cheque_id');

        $datalist=array(
            'paymnt_wholesale_id'=>0,
            'inv_no'=>$inv_no,
            'ord_no'=>$ord_no,
            'bill_no'=>$bill_no,
            'prty_name'=>$prtyname,
            'item_id'=>$itemids,
            'indvprice'=>$indvprice,
            'amount'=>$amount,
            'ser_tax'=>$ser_tax,
            'tot_amnt'=>$tot_amnt,
            'payed'=>$payed,
            'due_amount'=>$due_amount,
            'updated_by'=>1,
            'payment_type'=>$pay_type,
            'payment_type_id'=>$pay_type_id,
        );

//        print_r($datalist);exit();

        $exp=explode(",",$itemids);
        $indvpriceexp=explode(",",$indvprice);

        for($i=0;$i<sizeof($exp);$i++){

            $data['updtstat']=$this->Postmodel->update_status($exp[$i],'Delivered',$indvpriceexp[$i]);
            @$this->db->free_db_resource();
        }

        $data['savepayment']=$this->Postmodel->save_payment($datalist);
        @$this->db->free_db_resource();

        $data['savepayment']=$this->Postmodel->update_prty_due_blnce($prtyname,$finalamnt);
        @$this->db->free_db_resource();
    }

    function transaction_history(){

//        $num= $this->convert_number_to_words(78);

        $data['transaction_all']=$this->Postmodel->trancsaction_all($lim=50);
        @$this->db->free_db_resource();

//        print_r($data['transaction_all']);exit();

        $data['itemdetails']=$this->Postmodel->select_item();
        @$this->db->free_db_resource();

//        print_r($data['itemdetails']);exit();

//        Coment - These logic is not used as for now
/*        if(count($data['transaction_all']) == 0){

        }else{

            $arr = array();

            for($i=0;$i<count($data['transaction_all']);$i++){
                array_push($arr,$data['transaction_all'][$i]->item_id);
            }

            $string=implode(",",$arr);
            $itemid=explode(",",$string);

            for($i=0;$i<count($itemid);$i++){
                for($j=0;$j<count($data['itemdetails']);$j++){
                    if($data['itemdetails'][$j]->item_id == $itemid[$i]){

                        $datalist[$i]= array(
                            'item_id'=>$data['itemdetails'][$j]->item_id,
                            'dc_no'=>$data['itemdetails'][$j]->bill_no,
                            'ref_no'=>$data['itemdetails'][$j]->ref_no,
                            'product_type'=>$data['itemdetails'][$j]->product_type,
                            'product_name'=>$data['itemdetails'][$j]->product_name,
                            'quantity'=>$data['itemdetails'][$j]->quantity,
                            'price'=>$data['itemdetails'][$j]->price,
                        );
                    }
                }
            }

            $data['itemdetails']=$datalist;
        }*/

        $this->load->view('app/wholesale/transaction_history',$data);


    }

    function printpage(){

        $data['printdata'] = $this->Postmodel->get_print_data($_GET['gettext']);
        @$this->db->free_db_resource();
//        print_r($data['printdata']);exit();

        $this->load->view('app/wholesale/printpage',$data);

    }


    //AJAX CALL
    function getprodname(){

        $productName=array();

        $data['getprodname']=$this->Postmodel->get_prod_name($this->input->post('prod_type'));

        for($i=0;$i<count($data['getprodname']);$i++){

            array_push($productName,$data['getprodname'][$i]->prod_name);

//            print_r($productName);exit();
        }
        echo json_encode($productName);
    }

    function stocklistfetch(){
        $data['stocklist'] = $this->Postmodel->get_stock_list();
//        print_r($data['stocklist']);exit();
        @$this->db->free_db_resource();

        $this->load->view('ajaxcontent/stocklistfetch',$data);
    }

    function stocklistfetchspec(){

        $data['stocklist'] = $this->Postmodel->get_stock_list_spec($this->input->post('prod_name'),$this->input->post('sph'),$this->input->post('cyl'),
            $this->input->post('axis'),$this->input->post('addition'));

//        print_r($data['stocklist']);exit();

        @$this->db->free_db_resource();

        $this->load->view('ajaxcontent/stocklistfetch',$data);

    }

    function testdata(){

        $itemid=$this->input->post('itemid');
        $price=$this->input->post('price');

        $itemid_data=explode(',',$itemid);
        $price_data=explode(',',$price);
//        print_r($data);exit();

        $this->session->set_userdata('itemid',$itemid_data);
        $this->session->set_userdata('price',$price_data);

        echo $itemid;
    }

    function printinvoice(){

        $data['printinv'] = $this->Postmodel->itemdetails($this->input->post('item_id'));
        @$this->db->free_db_resource();

        $jsonencode=json_encode($data['printinv']);

        echo $jsonencode;
    }


    function change_status_outsource(){

        $selectcheckbox=$this->input->post('select_check');
        $status=$this->input->post('selectstat');

        if($selectcheckbox == ""){

            $this->session->set_flashdata('messageError', 'Please choose data for delete');
            redirect($this->agent->referrer());
        }else{

            $exp = implode(',', $selectcheckbox);

            $data['deldata']=$this->Postmodel->update_status_outsource($exp,$status);

            $this->session->set_flashdata('messageSuccess', 'Choosen Data Deleted');
            redirect($this->agent->referrer());
        }
    }

    /*function for changing the status from Ordered to Received  and also add quantity
to stock if the product type is READY item*/
    function change_status_purchase_order_list(){

        $selectcheckbox=$this->input->post('select_check');
        $status=$this->input->post('selectstat');

        if($selectcheckbox == ""){

            $this->session->set_flashdata('messageError', 'Please choose Data on Select Box');
            redirect($this->agent->referrer());
        }else{

            for($i=0;$i<count($selectcheckbox);$i++){

                $explodeval=explode(',',$selectcheckbox[$i]);

                $data['updatedata']=$this->Postmodel->update_status_purchageorderlist($explodeval[0],$status);

//                print_r($data['updatedata']);exit();

                if($explodeval[1] == 'READY' && $status == 'Received'){

                    $data=array(
                        'prod_name'=>$explodeval[2],
                        'sph'=>$explodeval[3],
                        'cyl'=>$explodeval[4],
                        'axis'=>$explodeval[5],
                        'addition'=>$explodeval[6],
                        'quant'=>$explodeval[7],
                    );

                    $data['addtostock']=$this->Postmodel->add_to_stock($data);

                }
            }

            $this->session->set_flashdata('messageSuccess', 'Data Transfered');
            redirect($this->agent->referrer());
        }
    }


/*function for changing the status from Received to Delivered To Counter and also remove quantity
from stock if the product type is READY item*/
    function change_status_received_order_list(){

        $selectcheckbox=$this->input->post('select_check');
        $status=$this->input->post('selectstat');

        if($selectcheckbox == ""){

            $this->session->set_flashdata('messageError', 'Please Select Atleast One Data For Transfer');
            redirect($this->agent->referrer());
        }else{

            for($i=0;$i<count($selectcheckbox);$i++){

                $explodeval=explode(',',$selectcheckbox[$i]);

                $data['updatedata']=$this->Postmodel->update_status_received_order_list($explodeval[0],$status);


                if($explodeval[1] == 'READY' && $status == 'Delivered To Counter'){

                    $data=array(
                        'prod_name'=>$explodeval[2],
                        'sph'=>$explodeval[3],
                        'cyl'=>$explodeval[4],
                        'axis'=>$explodeval[5],
                        'addition'=>$explodeval[6],
                        'quant'=>$explodeval[7],
                    );

                    $data['removefromstock']=$this->Postmodel->remove_from_stock($data);
                }
                if($explodeval[1] == 'READY' && $status == 'Bulk Purchase'){

                    $data=array(
                        'prod_name'=>$explodeval[2],
                        'sph'=>$explodeval[3],
                        'cyl'=>$explodeval[4],
                        'axis'=>$explodeval[5],
                        'addition'=>$explodeval[6],
                        'quant'=>$explodeval[7],
                    );

                    $data['addtostock']=$this->Postmodel->add_to_stock($data);
                }
            }

            $this->session->set_flashdata('messageSuccess', 'Data Transfered');
            redirect($this->agent->referrer());
        }
    }

    function checkbill(){
        $bill_no=$this->input->post('bill_no');

//        print_r($this->input->post('bill_no'));exit();
        $data['getbillno']=$this->Postmodel->gettingbillno($bill_no);
        @$this->db->free_db_resource();

        $count=count($data['getbillno']);

        echo $count;

    }


    function checkpartyname(){

        $party_name=$this->input->post('party_name');

        $data['getpartyname']= $this->Postmodel->getpartyname($party_name);
        @$this->db->free_db_resource();
// print_r($data['getpartyname']);exit();


        $count=count($data['getpartyname']);

        echo $count;

    }
}

?>


