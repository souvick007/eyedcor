<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

class Welcome extends CI_Controller {
    var $data;

    function __construct(){
        parent::__construct(); // needed when adding a constructor to a controller
        $this->data = array(
            'layoutmode' => $this->config->item('layoutconfigdev')
        );
        // $this->data can be accessed from anywhere in the controller.
        $this->load->model('Postmodel');
    }
    public function index()
    {

//        print_r($this->session->userdata());exit();
        $this->load->view('app/login');
    }

    function login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        if($username == "" && $password == ""){
            $this->session->set_flashdata('messageError', 'Please Provide Username & Password');
            redirect($this->agent->referrer());
        }
        if($username!=''&& $password ==""){

            $this->session->set_flashdata('messageError', 'Please Provide  Password');
            redirect($this->agent->referrer());
    }

        $data['logincred'] = $this->Postmodel->login_cred($username, $password);
        @$this->db->free_db_resource();

//        print_r($data['logincred'][0]->user_id);exit();

        if ($username == $data['logincred'][0]->user_id && $password == $data['logincred'][0]->password) {
            if ($data['logincred'][0]->user_type == 1) {


                //For admin pages
                $session_data =
                    array(
                        'user_id' => $data['logincred'][0]->user_id,
                        'password' => $data['logincred'][0]->password,
                        'user_type' => $data['logincred'][0]->user_type,
                    );

                $this->session->set_userdata('logged_in', $session_data);
                redirect("admin/product_type");

            } elseif($data['logincred'][0]->user_type == 2){

                $session_data =
                    array(
                        'user_id' => $data['logincred'][0]->user_id,
                        'password' => $data['logincred'][0]->password,
                        'user_type' => $data['logincred'][0]->user_type,
                    );

                $this->session->set_userdata('logged_in', $session_data);
                redirect("dashboard/counter");
            }else{
                $session_data =
                    array(
                        'user_id' => $data['logincred'][0]->user_id,
                        'password' => $data['logincred'][0]->password,
                        'user_type' => $data['logincred'][0]->user_type,
                    );

                $this->session->set_userdata('logged_in', $session_data);
                redirect("wholesale/sale");
            }


        }else {
            $this->session->set_flashdata('messageError', 'Please Provide Valid Username & Password');
            redirect($this->agent->referrer());
        }
    }

    function signup(){
        try{
        }
        catch (Exception $ex){
        }
    }
        
    function logout(){

        $this->session->sess_destroy();
        redirect('welcome');

    }
}
?>
