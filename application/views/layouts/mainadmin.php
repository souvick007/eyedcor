<h5 class="leftpanel-title">Navigation</h5>
<ul class="nav nav-pills nav-stacked">
    <li class="parent active"><a href=""><span  class="glyphicon  glyphicon- glyphicon-list">  Menu</span></a>
        <ul class="children">

            <li><a href="<?php echo base_url(); ?>admin/product_type">Product Type</a></li>
            <span><i class="fa fa-dot-circle-o paddingicon " aria-hidden="true"></i></span><span class="menuheadstyle">Product Name</span>
            <li><a href="<?php echo base_url(); ?>admin/product_name">Ready/Rx</a></li>
            <li><a href="<?php echo base_url(); ?>admin/product_name_cont">Contact Lens</a></li>
            <li><a href="<?php echo base_url(); ?>admin/product_name_grind">Grinding</a></li>
            <li><a href="<?php echo base_url(); ?>admin/product_name_soln">Solution</a></li>
            <li><a href="<?php echo base_url(); ?>admin/product_name_acc">Accessory</a></li>
            <li><a href="<?php echo base_url(); ?>admin/party_name">Party Name</a></li>
            <li><a href="<?php echo base_url(); ?>admin/search_list">Seach List</a></li>
            <li><a href="<?php echo base_url(); ?>admin/sales_report">Sales Report</a></li>
            <li><a href="<?php echo base_url(); ?>admin/deliv_list_manage">Delivery List Management</a></li>
        </ul>
    </li>
</ul>