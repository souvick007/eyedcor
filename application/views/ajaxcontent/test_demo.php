<div>
<div class="col-md-12">
    <input type="hidden" id="valtxt">

    <center><div class="txtalgn">RETAIL INVOICE</div></center>
    <center><font size="">ORIGINAL : BUYER'S COPY</font></center>
    <center><font size="">VALID FOR INPUT TAX CREDIT</font></center>
    <h1><center><font size="">EYE DECOR</font></center></h1>
    <center><div class="txtalgn">WHOLE SALE OPTICIAN</div></center>
    <center><div class="txtalgn">S C GOSWAMI ROAD, PANBAJAR</div></center>
    <center><div class="txtalgn">GUWAHATI-781001</div></center>
    <center><div class="txtalgn">VAT NO:18570202998. CST NO 18949944830</div></center>

</div>
<div class="col-md-12">

    <table style="width:100%">

        <tr>
            <td>Buyer's Name :<span id="prty_nme"></span></td>

            <td>TIN :</td>
            <td></td>



        </tr>
        <tr>
            <td>Address :<span id="prty_adrs"></span></td>
            <td>Cst Regd No :</td>
            <td></td>


        </tr>
        <tr>
            <td></td>
            <td>Challan No :<span id="ord_no"</td>
            <td>Date:</td>

        </tr>
        <tr>
            <td>Phone:</td>
            <td>RR/VPP Regd Air No</td>
            <td></td>

        </tr>

    </table>

    <h2></h2>

    <input type="hidden" id="item_id">

    <table style="margin-bottom: 15px !important;width:100%">
        <tr>
            <th><center>SL.NO</center></th>
            <th><center>DC NO</center></th>
            <th><center>REF NO</center></th>
            <th><center>TYPE</center></th>
            <th><center>ITEM</center></th>
            <th><center>Qnty.</center></th>
            <th><center>Price Per unit</center></th>
            <th colspan="4"><center>VALUE OF GOOD QUANTITY PRICE PER UNIT</center></th>

        </tr>
        <tr>

            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <th colspan="2"><center>Amount</center></th>
        </tr>

        <tr>
            <td><?php echo count($this->session->userdata('itemid'));?></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <th><center>Rs</center></th>
            <th><center>P</center></th>
        </tr>

        <?php for($i=0;$i<count($itemdetails);$i++){ ?>
            <?php for($j=0;$j<count($this->session->userdata('itemid'));$j++){ ?>
                <?php if($itemdetails[$i]['item_id'] == $this->session->userdata('itemid')[$j]){ ?>

                    <tr>
                        <td><?php echo $i + 1;?></td>
                        <td><?php echo $itemdetails[$i]['dc_no'];?></td>
                        <td><?php echo $itemdetails[$i]['ref_no'];?></td>
                        <td><?php echo $itemdetails[$i]['product_type'];?></td>
                        <td><?php echo $itemdetails[$i]['product_name'];?></td>
                        <td><?php echo $itemdetails[$i]['quantity'];?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?php echo $this->session->userdata('price')[$j];?></td>
                        <td></td>

                    </tr>
                <?php } ?>
            <?php } ?>
        <?php } ?>

        <tr>
            <td rowspan="3"></td>
            <td colspan="4"><center>TOTAL VALUE</center></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><span id="billing_amnt"></span></td>
            <td></td>
        </tr>

        <tr>

            <td colspan="4"><center>FREIGHT & OTHER SERVICES</center></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><span id="othr_srv"></span></td>
            <td></td>
        </tr>


        <tr>
            <td colspan="4"><center>TOTAL PAYBLE AMOUNT</center></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><span id="total_amnt"></span></td>
            <td></td>
        </tr>


    </table>


    Total Amount (in word) Rupees.</br>
    E.&O.E


</div>
<div class="col-md-12">

    <div class="col-md-10">
        <div class="">


            <nav>
                <ul>
                    "I/We hereby certify that my/our registration certificate under the Assam Value Added Tax 2003
                    Is in force on the date on which the sale of the good specified in this tax invoice has been effected
                    By me/us."</br>

                    Goods ordered and not dispatched are out of stock. We are not responsible for any loss or
                    breakage On transit interest @ 18% will be charged if not paid on presentation
                </ul>
            </nav>
        </div>
    </div>
    <div class="col-md-2">
        <article>
            For EYE DECOR</br></br>

            Authorised Signatory
        </article>
    </div>
</div>
</div>
