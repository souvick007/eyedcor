<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/app.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery_msg.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/buttons.html5.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/buttons.print.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/pdfmake.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jszip.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/vfs_fonts.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/buttons.flash.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/required_utilization.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap-dialog.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/menu.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/notify.js"></script>
<!--<script type="text/javascript" src="https://cdn.datatables.net/tabletools/2.2.2/swf/copy_csv_xls_pdf.swf"></script>-->
<!--<script type="text/javascript" src="--><?php //echo base_url(); ?><!--js/jquery-1.12.4.js"></script>-->

	
