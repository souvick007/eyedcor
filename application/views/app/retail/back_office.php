<?php $this->load->view('assets/css'); ?>


<head>
    <style>
        a:hover {
            background-color: yellow;
        }
    </style>
</head>

<?php $this->load->view('layouts/header'); ?>
<section>
    <div class="mainwrapper">
        <div class="leftpanel">
            <?php $this->load->view('layouts/main'); ?>
        </div><!-- leftpanel -->
        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">BACK OFFICE</a></li>
                        </ul>
                        <h4>BACK OFFICE <button class="btn btn-sm btn-info margin7" style="margin-left: 5%;">GO PURCHASE ORDER</button></h4>
                    </div>
                </div><!-- media -->
            </div>

            <br/>
            <div class="row">
                <div class="col-lg-6">
                    <div>
                       <div class="form-horizontal">
                          <div class="form-group ">
                              <label class="col-sm-4 control-label" for="lg">CUSTOMER ID</label>
                                  <div class="col-sm-8">
                                      <input class="form-control" type="text" id="lg">
                                        </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label" for="lg">ORDER ID</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="lg">
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label" for="lg">CUSTOMER NAME</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text">
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">TYPE OF LENS</label>
                                            <div class="col-sm-8">
                                                <select  class="form-control"  id="ddlTrackCycle" title="Select Track Cycle">
                                                    <option value="" selected="selected">SELECT TYPE OF LENS</option>
                                                    <option value="SINGLE VISION">SINGLE VISION</option>
                                                    <option value="SINGLE VISION ARC">SINGLE VISION ARC</option>
                                                    <option value="SINGLE VISION ARC PG">SINGLE VISION ARC PG</option>
                                                    <option value="BIFOD">BIFOD</option>
                                                    <option value="BIFOD ARC">BIFOD ARC</option>
                                                    <option value="BIFOD ARC PG">BIFOD ARC PG</option>
                                                    <option value="PROGRESSIVE">PROGRESSIVE</option>
                                                    <option value="DIGITAL PROGRESSIVE">DIGITAL PROGRESSIVE</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">LENS COMPANY</label>
                                            <div class="col-sm-8">
                                                <select  class="form-control"  id="ddlTrackCycle" title="Select Track Cycle">
                                                    <option value="" selected="selected">SELECT COMPANY</option>
                                                    <option value="GKB">GKB</option>
                                                    <option value="SLR">SLR</option>
                                                    <option value="OTHER">OTHER</option>
                                                    <option value="OWN STOCK">OWN STOCK</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label" for="lg">SPECIFY COMPANY NAME</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text">
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">SPECIFICATION</label>
                                            <div class="col-sm-8">
                                                <select  class="form-control"  id="ddlTrackCycle" title="Select Track Cycle">
                                                    <option value="" selected="selected">SELECT SPECIFICATION</option>
                                                    <option value="TRANSITION CRIZAL A2 BROWN" >TRANSITION CRIZAL A2 BROWN</option>
                                                    <option value="TRANSITION CRIZAL A2 GRAY">TRANSITION CRIZAL A2 GRAY</option>
                                                    <option value="XTERLITE SATIN">XTERLITE SATIN</option>
                                                    <option value="XTERLITE SHMC">XTERLITE SHMC</option>
                                                    <option value="XTERLITE TC TITENUM COAT">XTERLITE TC TITENUM COAT</option>
                                                    <option value="RGLR SV HC">RGLR SV HC</option>
                                                    <option value="RGLR SV ARC">RGLR SV ARC</option>
                                                    <option value="RGLR SV UC">RGLR SV UC</option>
                                                </select>
                                            </div>
                                        </div>



                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label" for="lg">AMOUNT</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="lg">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label" >DELIVERY STATUS</label>
                                            <div class="col-sm-8">
                                                <select  class="form-control"  id="ddlTrackCycle" title="Select Track Cycle">
                                                    <option value="" selected="selected">SELECT STATUS</option>
                                                    <option value="NOT DELIVERED">NOT DELIVERED</option>
                                                    <option value="PARTIAL DELIVERED">PARTIAL DELIVERED</option>
                                                    <option value="DELIVERED">DELIVERED</option>
                                                    <option value="CANCELLED">CANCELLED</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label" >BACK OFFICE STATUS</label>
                                            <div class="col-sm-8">
                                                <select  class="form-control"  id="ddlTrackCycle" title="Select Track Cycle">
                                                    <option value="" selected="selected">SELECT STATUS</option>
                                                    <option value="IN PROGRESS">IN PROGRESS</option>
                                                    <option value="PARTIAL DELIVERED IN COUNTER">PARTIAL DELIVERED IN COUNTER</option>
                                                    <option value="DELIVERED IN COUNTER">DELIVERED IN COUNTER</option>
                                                    <option value="OUT STATION ORDER">OUT STATION ORDER</option>
                                                    <option value="CANCELLED">CANCELLED</option>
                                                </select>
                                            </div>
                                        </div>


                                        <div class="form-group ">

                                            <table class="tatble_style">
                                                <tr class="row_style">
                                                    <td style="border: solid #000000 2px;padding: 0 !important;" colspan="2"><b>LENS MEASUREMENT</b></td>
                                                    <!--<td style="border: solid #000000 2px"></td>-->
                                                    <td class="column_style" style="padding: 0 !important;">SPH</td>
                                                    <td class="column_style" style="padding: 0 !important;">CYL</td>
                                                    <td class="column_style" style="padding: 0 !important;">AXIS</td>
                                                    <td class="column_style" style="padding: 0 !important;">V/A</td>
                                                </tr>
                                                <tr class="row_style">
                                                    <td class="column_style" rowspan="2">RIGHT EYE</td>
                                                    <td class="column_style">DISTANCE</td>
                                                    <td class="column_style"><input type="text" class="input_column"/></td>
                                                    <td class="column_style"><input type="text" class="input_column"/></td>
                                                    <td class="column_style"><input type="text" class="input_column"/></td>
                                                    <td class="column_style"><input type="text" class="input_column"/></td>
                                                </tr>

                                                <tr class="row_style">
                                                    <!--<td style="border: solid #000000 2px"></td>-->
                                                    <td class="column_style">NEAR ADD</td>
                                                    <td class="column_style"><input type="text" class="input_column"/></td>
                                                    <td class="column_style"><input type="text" class="input_column"/></td>
                                                    <td class="column_style"><input type="text" class="input_column"/></td>
                                                    <td class="column_style"><input type="text" class="input_column"/></td>
                                                </tr>

                                                <tr class="row_style">
                                                    <td class="column_style" rowspan="2">LEFT EYE</td>
                                                    <td class="column_style">DISTANCE</td>
                                                    <td class="column_style"><input type="text" class="input_column"/></td>
                                                    <td class="column_style"><input type="text" class="input_column"/></td>
                                                    <td class="column_style"><input type="text" class="input_column"/></td>
                                                    <td class="column_style"><input type="text" class="input_column"/></td>
                                                </tr>
                                                <tr class="row_style">
                                                    <td class="column_style">NEAR ADD</td>
                                                    <td class="column_style"><input type="text" class="input_column"/></td>
                                                    <td class="column_style"><input type="text" class="input_column"/></td>
                                                    <td class="column_style"><input type="text" class="input_column"/></td>
                                                    <td class="column_style"><input type="text" class="input_column"/></td>
                                                </tr>

                                            </table>

                                        </div>




                                        <div class="pull-right" >
                                            <button class="btn btn-sm btn-info margin7">Modify Data</button>
                                            <button class="btn btn-sm btn-info margin7">Save</button>
                                            <button class="btn btn-sm btn-info margin7 " >Cancel</button>
                                        </div>
                                    </div>
                                </div>
                </div>

                <div class="col-lg-6" >

                    <div class="col-md-12">

                        <div class="col-sm-4">
                            <div class="form-group">

                                <select  class="form-control"  id="ddlTrackCycle" title="Select Track Cycle">
                                    <option value="" selected="selected">SELECT COMPANY</option>
                                    <option value="GKB">GKB</option>
                                    <option value="SLR">SLR</option>
                                    <option value="OTHER">OTHER</option>
                                    <option value="OWN STOCK">OWN STOCK</option>
                                </select>

                                <!--<input class="form-control" type="text" id="CMCompanyName" placeholder="CUSTOMAR ID" maxlength="30" readonly/>-->

                            </div><!-- form-group -->
                        </div><!-- col-sm-6 -->
                        <div class="col-sm-4">
                            <div class="form-group">

                                <select  class="form-control"  id="ddlTrackCycle" title="Select Track Cycle">
                                    <option value="" selected="selected">Please Select Type of Lens</option>
                                    <option value="SINGLE VISION">SINGLE VISION</option>
                                    <option value="SINGLE VISION ARC">SINGLE VISION ARC</option>
                                    <option value="SINGLE VISION ARC PG">SINGLE VISION ARC PG</option>
                                    <option value="BIFOD">BIFOD</option>
                                    <option value="BIFOD ARC">BIFOD ARC</option>
                                    <option value="BIFOD ARC PG">BIFOD ARC PG</option>
                                    <option value="PROGRESSIVE">PROGRESSIVE</option>
                                    <option value="DIGITAL PROGRESSIVE">DIGITAL PROGRESSIVE</option>
                                </select>

                            </div><!-- form-group -->
                        </div><!-- col-sm-6 -->
                        <div class="col-sm-4">
                            <div class="form-group">

                                <select  class="form-control"  id="ddlTrackCycle" title="Select Track Cycle">
                                    <option value="" selected="selected">Please Select Specification</option>
                                    <option value='TRANSITION CRIZAL A2 BROWN' >TRANSITION CRIZAL A2 BROWN</option>
                                    <option value='TRANSITION CRIZAL A2 GRAY'>TRANSITION CRIZAL A2 GRAY</option>
                                    <option value="XTERLITE SATIN">XTERLITE SATIN</option>
                                    <option value="XTERLITE SHMC">XTERLITE SHMC</option>
                                    <option value="XTERLITE TC TITENUM COAT">XTERLITE TC TITENUM COAT</option>
                                    <option value="RGLR SV HC">RGLR SV HC</option>
                                    <option value="RGLR SV ARC">RGLR SV ARC</option>
                                    <option value="RGLR SV UC">RGLR SV UC</option>
                                </select>
                                <!--<input class="form-control" type="text" id="CMCompanyName" placeholder="CUSTOMAR ID" maxlength="30" ng-model="list_company.name"  readonly/>-->

                            </div><!-- form-group -->
                        </div><!-- col-sm-6 -->


                    </div>


                    <hr/>

                        <div>
                                <div>
                                    <div class="input-group">
                                        <input type="text" class="form-control">
                    <span class="input-group-addon">
                        <i class="fa fa-search"></i>
                    </span>
                                    </div>
                                </div>
                                <div class="medium no-padding" id="gridscroll" style="height: 425px; overflow: scroll;">
                                        <div>
                                            <table class="table fontsizestyle"><thead>
                                                <tr>
                                                    <th class="hath">&nbsp;</th>
                                                    <th class="hath">COMPANY</th>
                                                    <th class="hath">LENS TYPE</th>
                                                    <th class="hath">SPECIFICATION</th>
                                                    <th class="hath">ORDER_DATE</th>
                                                    <th class="hath">DELIVERY DATE</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td class="text-center"><input type="radio" name="id"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                        </div>
                </div>
            </div>
        </div>

        <?php $this->load->view('layouts/footer'); ?>
    </div>
</section>