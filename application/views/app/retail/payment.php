<?php $this->load->view('assets/css'); ?>

<head>
    <style>
        a:hover {
            background-color: yellow;
        }
    </style>
</head>

<?php $this->load->view('layouts/header'); ?>
<section>
    <div class="mainwrapper">

        <div class="leftpanel">
            <?php $this->load->view('layouts/main'); ?>
        </div><!-- leftpanel -->

        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">PAYMENT</a></li>
                        </ul>
                        <h4>PAYMENT
                        </h4>
                    </div>
                </div><!-- media -->
            </div>

            <br/>
            <div class="row ">

                <div class="col-lg-6">
                    <srd-widget>
                        <div class="widget" >
                            <srd-widget-header icon="fa-tasks" title="Servers" class=" ">
                            </srd-widget-header>
                            <srd-widget-body classes="medium no-padding" class=" ">
                                <div class="widget-content"  >
                                    <div class="form-horizontal">

                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">CUSTOMER ID</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text"  >
                                            </div>
                                        </div>

                                        <div class="form-group" >
                                            <label class="col-sm-4 control-label">ORDER ID</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" />
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="col-sm-4 control-label">NAME</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="CMCompanyName" placeholder="Company Name" maxlength="30" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label" >MOBILE NUMBER</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="CMCompanyName" placeholder="Current Age" maxlength="30" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">TOTAL CHARGE</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" placeholder="TOTAL CHARGE" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">ADVANCE</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="emailid" placeholder="ADVANCE"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">DUE AMOUNT</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="emailid" placeholder="DUE AMOUNT"/>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <form>
                                                <label class="col-sm-4 control-label"><input type="radio"  value="cash" > PAYMENT BY CASH</label>

                                                <label class="col-sm-4 control-label"><input type="radio" value="card" > PAYMENT BY CARD</label>
                                            </form>
                                        </div>

                                        <!--<div class="form-group" >-->

                                        <!--<label class="col-sm-4 control-label">FULL DUE RECEIVED<input type="checkbox"   style="margin-left: 20px"/></label>-->
                                        <!--</div>-->

                                        <div class="form-group " >
                                            <form>
                                                <label class="col-sm-4 control-label"><input type="radio"value="full_collect" > FULL DUE RECEIVED</label>

                                                <label class="col-sm-4 control-label"><input type="radio"  value="discount" > DISCOUNT ON DUE</label>
                                            </form>
                                        </div>

                                        <div class="form-group" >
                                            <label class="col-sm-4 control-label">FINAL DUE RECEIVED</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="emailid" placeholder="FINAL DUE RECEIVED" />
                                            </div>
                                        </div>

                                        <div class="form-group" >
                                            <label class="col-sm-4 control-label">FINAL DISCOUNT ON DUE</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="emailid" placeholder="FINAL DUE RECEIVED" />
                                            </div>
                                        </div>

                                        <div class="pull-right">
                                            <input type="button" class="btn btn-primary" id="btnsave1"  value="Save" />
                                            <input type="button" class="btn btn-primary" id="btnsave1" value="Update" />
                                            <input type="button" class="btn btn-primary" id="btnCancelComp" value="Cancel" />
                                            <input type="button" class="btn btn-primary" id="btnCancelComp" value="Print Bill" />
                                        </div><!-- panel-footer -->
                                    </div>
                                </div>
                            </srd-widget-body>
                        </div>
                    </srd-widget>
                </div>

                <div class="col-lg-6" >
                        <div class="widget" >
                                    <div class="input-group">
                                        <input type="text" class="form-control"  placeholder="Search" >
                    <span class="input-group-addon" style="cursor: pointer">
                        <i class="fa fa-search"></i>
                    </span>
                                    </div>
                                <div class="medium no-padding" id="gridscroll" style="height: 425px; overflow: scroll;" ">
                                        <div class="table-responsive ">
                                            <table class="table fontsizestyle"><thead>
                                                <tr>
                                                    <th >&nbsp;</th>
                                                    <th class="hath" >CUSTOMER ID</th>
                                                    <th class="hath" >ORDER ID</th>
                                                    <th class="hath" >PAYMENT STATUS</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td class="text-center"><input type="radio" name="id" ></td>
                                                    <!--<td>TEST LENCE</td>-->
                                                    <td></td>

                                                    <td></td>
                                                    <td></td>
                                                </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                </div>
                </div>
            </div>

            <!--SECTOIN ADDED FOR PRINT BILL-----START-->

<!--            <div id="printableArea" >-->
<!---->
<!--                <body class="body_class">-->
<!--                <header class="clearfix">-->
<!--                    <div id="logo">-->
<!--                        <img src="images/logo.png">-->
<!--                    </div>-->
<!--                    <h1 class="h1_class">INVOICE</h1>-->
<!--                    <div id="company" class="clearfix">-->
<!--                        <div>OPTICAL SOLUTIONS</div>-->
<!--                        <div>455, Rajdanga<br /> KOLKATA, INDIA</div>-->
<!--                        <div>(033) 519-0450</div>-->
<!--                        <div>opticalsolutions@example.com</div>-->
<!--                        <!--<div><a href="mailto:company@example.com">company@example.com</a></div>-->
<!--                    </div>-->
<!--                    <div id="project">-->
<!--                        <div><span>INVOICE TO:-</span></div>-->
<!--                        <div><span>PATIENT NAME:</span> {{payment_data.customer_name}}</div>-->
<!--                        <div><span>PATIENT ID:</span> {{payment_data.customer_id}}</div>-->
<!--                        <div><span>ODER ID:</span> {{payment_data.order_id}}</div>-->
<!--                        <div><span>MOBILE NUMBER:</span> {{payment_data.mobile_number}}</div>-->
<!--                        <div><span>ADDRESS:</span> {{payment_data.address}}</div>-->
<!--                        <!--<div><span>DUE DATE</span> September 17, 2015</div>-->
<!--                    </div>-->
<!--                </header>-->
<!--                <main>-->
<!--                    <table class="table_class">-->
<!--                        <thead>-->
<!--                        <tr>-->
<!--                            <th class="service">PRODUCT</th>-->
<!--                            <th class="desc">DESCRIPTION</th>-->
<!--                            <th>PRICE</th>-->
<!--                            <th>QTY</th>-->
<!--                            <th>TOTAL</th>-->
<!--                        </tr>-->
<!--                        </thead>-->
<!--                        <tbody>-->
<!--                        <tr >-->
<!--                            <td class="service">FRAME</td>-->
<!--                            <td class="desc">{{payment_data.frame_code}},{{payment_data.frame_type}}{{payment_data.frame_company}}</td>-->
<!--                            <td class="unit"><i class="fa fa-inr"></i>{{payment_data.frame_amount}}</td>-->
<!--                            <td class="qty">1</td>-->
<!--                            <td class="total"><i class="fa fa-inr"></i>{{payment_data.frame_amount}}</td>-->
<!--                        </tr>-->
<!---->
<!--                        <tr >-->
<!--                            <td class="service">CONTACT LENS</td>-->
<!--                            <td class="desc">{{payment_data.contact_lens_company}}</td>-->
<!--                            <td class="unit"><i class="fa fa-inr"></i>{{payment_data.contact_lens_amount}}</td>-->
<!--                            <td class="qty">1</td>-->
<!--                            <td class="total"><i class="fa fa-inr"></i>{{payment_data.contact_lens_amount}}</td>-->
<!--                        </tr>-->
<!---->
<!--                        <tr >-->
<!--                            <td class="service">LENS</td>-->
<!--                            <td class="desc">{{payment_data.lens_type}},{{payment_data.lens_specification}},{{payment_data.lens_company}}</td>-->
<!--                            <td class="unit"><i class="fa fa-inr"></i>{{payment_data.lens_amount}}</td>-->
<!--                            <td class="qty">1</td>-->
<!--                            <td class="total"><i class="fa fa-inr"></i>{{payment_data.lens_amount}}</td>-->
<!--                        </tr>-->
<!--                        <td colspan="4">SUBTOTAL</td>-->
<!--                        <td class="total"><i class="fa fa-inr"></i>{{payment_data.total_amount}}</td>-->
<!--                        </tr>-->
<!---->
<!--                        <tr >-->
<!--                            <td colspan="4">Vat {{payment_data.vat_percentage}} %</td>-->
<!--                            <td class="total"><i class="fa fa-inr"></i>{{payment_data.total_amount*(payment_data.vat_percentage/100)}}</td>-->
<!--                        </tr>-->
<!---->
<!--                        <tr>-->
<!--                            <td colspan="4">DISCOUNT</td>-->
<!--                            <td class="total"><i class="fa fa-inr"></i>{{payment_data.discount}}</td>-->
<!--                        </tr>-->
<!---->
<!--                        <tr >-->
<!--                            <td colspan="4">FINAL DISCOUNT</td>-->
<!--                            <td class="total"><i class="fa fa-inr"></i>{{payment_data.final_due_discount}}</td>-->
<!--                        </tr>-->
<!---->
<!--                        <tr >-->
<!--                            <td colspan="4">TOTAL DISCOUNT</td>-->
<!--                            <td class="total"><i class="fa fa-inr"></i>{{payment_data.final_due_discount+payment_data.discount}}</td>-->
<!--                        </tr>-->
<!---->
<!--                        <tr>-->
<!--                            <td colspan="4" class="grand total">GRAND TOTAL</td>-->
<!--                            <td class="grand total"><i class="fa fa-inr"></i>{{payment_data.total_charge}}</td>-->
<!--                        </tr>-->
<!---->
<!--                        <tr >-->
<!--                            <td colspan="4" class="grand total">ADVANCE AMOUNT</td>-->
<!--                            <td class="grand total"><i class="fa fa-inr"></i>{{payment_data.advance}}</td>-->
<!--                        </tr>-->
<!---->
<!--                        <tr >-->
<!--                            <td colspan="4" class="grand total">DUE AMOUNT</td>-->
<!--                            <td class="grand total"><i class="fa fa-inr"></i>{{payment_data.due_amount}}</td>-->
<!--                        </tr>-->
<!--                        </tbody>-->
<!--                    </table>-->
<!--                </main>-->
<!--                <footer class="footer_class">-->
<!--                    Invoice was created on a computer and is not valid without the signature and seal.-->
<!--                </footer>-->
<!--                </body>-->
<!---->
<!---->
<!---->
<!---->
<!--            </div>-->

            <!--SECTOIN ADDED FOR PRINT BILL-----END-->
        </div>

        <?php $this->load->view('layouts/footer'); ?>

    </div>
</section>