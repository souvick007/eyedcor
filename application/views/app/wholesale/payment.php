<?php $this->load->view('assets/css'); ?>

<?php $this->load->view('assets/js'); ?>

<?php $this->load->view('layouts/header'); ?>



<section>
    <div class="mainwrapper">
        <div class="leftpanel" style="padding-left: 0px !important;">
            <div id = "myDiv"  class="loader" style="display:none;opacity: 0.5"><img id = "myImage" ></div>
            <?php $this->load->view('layouts/mainwholesale'); ?>
        </div><!-- leftpanel -->


        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="media-body">

                        <div id="notification"  style="display: none;position: absolute;top: 3px;right: 1px;width: 28%;z-index: 105;text-align: center;font-size: 14px;font-weight: 700;color: white;background-color: #60b544;padding: 9px;">Data Saved Successfully </div>

                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">PAYMENT </a></li>
                        </ul>
                        <h4>PAYMENT
                        </h4>
                    </div>
                </div><!-- media -->
            </div>
            <br/>

            <div style="position: absolute;top: 1px;right: 46px;"><label>Due Amount : <?php echo $due_amt; ?> </label></div>
            <input type="hidden" id="tot_due_amnt" value="<?php echo $due_amt; ?> "/>
<!--            <input type="hidden" id="setval"/>-->

            <?php for($i=0;$i<count($itemdetails);$i++){?>

                <?php if($i>0 && $itemdetails[$i]->sales_order_id == $itemdetails[$i-1]->sales_order_id){?>

                    <?php continue;?>
                <?php }else{ ?>

                    <div class="col-md-12">

                        <div class="col-md-3"><label>Invoice No :  <span id="inv_no" ><?php echo $inv_no ?></span></label></div>
                        <div class="col-md-3"><label>Order No : <span id="ord_no" ><?php echo $itemdetails[$i]->sales_order_id; ; ?></span> </label></div>
                        <div class="col-md-3"><label>Bill No : <span id="bill_no" ><?php echo $itemdetails[$i]->bill_no; ; ?></span></label></div>
                        <div class="col-md-3"><label>Party Name : <?php echo $itemdetails[0]->party_name; ?> </label></div>
                        <div ><input type="hidden" name="hid_prty_name" id="hid_prty_name" value="<?php echo $itemdetails[0]->party_name; ?>"/></div>
                    </div>

                    <div class="col-md-12" style="border-bottom: dashed 1px black "></div>

                <?php } ?>

            <?php }?>

            <div class="col-md-12">
                <div class="col-md-8">
                    <div style="height: 250px;">
                        <table class="table">
                            <thead>
                            <tr>
                                <th class="hath" style="font-size: 11px">SL NO</th>
                                <th class="hath" style="font-size: 11px">PRODUCT TYPE</th>
                                <th class="hath" style="font-size: 11px">PRODUCT NAME</th>
                                <th class="hath" style="font-size: 11px">ITEM DETAILS </th>
                                <th class="hath" style="font-size: 11px">QUANTITY</th>
                                <th class="hath" style="font-size: 11px">TOTAL COST</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php $i=0;?>
                            <?php foreach($itemdetails as $val):?>
                                <tr style="font-size: 10px;">
                                    <td><?php echo $i + 1;?></td>
                                    <td><?php echo $val->product_type;?></td>
                                    <td><?php echo $val->product_name;?></td>
                                    <?php if($val->product_type == 'READY'){ ?>
                                        <td>
                                            sph:<?php echo $val->sph;?> &nbsp;cyl:<?php echo $val->cyl;?>&nbsp; axis:<?php echo $val->axis;?> &nbsp;addition:<?php echo $val->addition;?>
                                            <br>
                                            side:<?php echo $val->side;?>&nbsp;&nbsp;remarks:<?php echo $val->remarks;?>
                                        </td>
                                    <?php }elseif($val->product_type == 'RX'){ ?>
                                        <td>
                                            sph:<?php echo $val->sph;?> &nbsp;cyl:<?php echo $val->cyl;?>&nbsp; axis:<?php echo $val->axis;?> &nbsp;addition:<?php echo $val->addition;?>
                                            <br>
                                            side:<?php echo $val->side;?>&nbsp;&nbsp;remarks:<?php echo $val->remarks;?>
                                            <br>
                                            specification:<?php echo $val->specification;?>
                                        </td>
                                    <?php }elseif($val->product_type == 'GRINDING'){ ?>
                                        <td>
                                            sph:<?php echo $val->sph;?> &nbsp;cyl:<?php echo $val->cyl;?>&nbsp; axis:<?php echo $val->axis;?> &nbsp;addition:<?php echo $val->addition;?>
                                            <br>
                                            side:<?php echo $val->side;?>&nbsp;diameter:<?php echo $val->diameter;?>&nbsp;base:<?php echo $val->base;?>&nbsp;remarks:<?php echo $val->remarks;?>
                                            <br>
                                            specification:<?php echo $val->specification;?>
                                        </td>
                                    <?php }elseif($val->product_type == 'CONTACT LENS'){ ?>
                                        <td>
                                            sph:<?php echo $val->sph;?> &nbsp;cyl:<?php echo $val->cyl;?>&nbsp; axis:<?php echo $val->axis;?> &nbsp;addition:<?php echo $val->addition;?>
                                            <br>
                                            side:<?php echo $val->side;?>&nbsp;&nbsp;remarks:<?php echo $val->remarks;?>
                                            <br>
                                            specification:<?php echo $val->specification;?>
                                        </td>
                                    <?php }elseif($val->product_type == 'SOLUTION'){ ?>
                                        <td>
                                            remarks:<?php echo $val->remarks;?>
                                        </td>
                                    <?php }elseif($val->product_type == 'ACCESSORY'){ ?>
                                        <td>
                                            remarks:<?php echo $val->remarks;?>
                                        </td>
                                    <?php }else{ ?>
                                        <td>
                                            remarks:<?php echo $val->remarks;?>
                                        </td>
                                    <?php } ?>


                                    <td><?php echo $val->quantity;?></td>
                                    <input type="hidden" id="quantity<?php echo $val->id;?>" value="<?php echo $val->quantity;?>">
                                    <td><input type="number" name="total_cost" id="total_cost" class="totalprice" value="<?php echo $val->price;?>" style="width:50px" ></td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                        <div class="col-md-3"><label>Party Address : <?php echo $address; ?> </label></div>
                        <div ><input type="hidden" name="hid_prty_adrs" id="hid_prty_adrs" value="<?php echo $address; ?>"/></div>
                    </div>
                </div>
                <div class="col-md-4" style="margin-top: 40px">

                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <div class="col-md-8">
                            AMOUNT
                        </div>
                        <div class="col-md-4">
                            <input type="text" name="amount" id="amount" style="width: 60px" class="form-control eilmlitecontrols more" disabled>
                            <input type="hidden" name="amount_hid" id="amount_hid" />
                        </div>
                    </div>

                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <div class="col-md-8">
                            SERVICE TAX
                        </div>
                        <div class="col-md-4">
                            <input type="text"  name="serv_tax" id="serv_tax" style="width: 60px"   onkeyup="service()" class="form-control eilmlitecontrols more">
                            <input type="hidden" name="serv_tax_hid" id="serv_tax_hid" />
                        </div>
                    </div>

                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <div class="col-md-8">
                            TOTAL AMOUNT
                        </div>
                        <div class="col-md-4">
                            <input type="text" style="width: 60px" name="tot_amount" id="tot_amount" class="form-control eilmlitecontrols more" disabled />
                        </div>
                    </div>


                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <div class="col-md-8">
                            DISCOUNT
                        </div>
                        <div class="col-md-4">
                            <input type="text" style="width: 60px" name="discnt_amount" id="discnt_amount" class="form-control eilmlitecontrols more" >
                        </div>
                    </div>



                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <div class="col-md-8">
                            PAID AMOUNT
                        </div>
                        <div class="col-md-4">
                            <input type="text" style="width: 60px" name="paid_amount" id="paid_amount" onkeyup="paid_amount()" class="form-control eilmlitecontrols more" />
                        </div>
                    </div>

                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <div class="col-md-8">
                            DUE AMOUNT
                        </div>
                        <div class="col-md-4">
                            <input type="text" name="due_amount" id="due_amount" style="width: 60px" class="form-control eilmlitecontrols more" disabled/>
                            <input type="hidden"  name="flag_val" id="flag_val" />
                            <input type="hidden"  name="item_id" id="item_id" />
                            <input type="hidden"  name="item_ids_get" id="item_ids_get" value="<?php echo $item_ids; ?>"/>
                        </div>
                    </div>

                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <div class="col-md-8">
                            PAY MODE
                        </div>
                        <div class="col-md-4">
                            <select style=" font-size: 12px;" name="paymode" id="paymode" onchange="paymodestat()">
                                <option value ="Cash">CASH</option>
                                <option value="Cheque">CHEQUE</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <div class="col-md-8">
                            CHEQUE NO.
                        </div>
                        <div class="col-md-4">
                            <input type="text" style="width: 190%;" name="cheque_id" id="cheque_id" disabled>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-12">

                <div class="pull-right" style="margin-bottom: 68px;">
                    <input type="button" onclick="returnval()" class="btn btn-primary" id="btnsave" value="Save"/>
                </div>
                <div class="pull-right" style="margin-bottom: 68px;margin-right: 100px;">
                    <input type="checkbox" name="delivered" id="delivered" value="Delivered">Delivered
                </div>
            </div>
        </div>
    </div>


</section>

<script type="text/javascript">

    function printDivReceipt(divName){

        var originalContents = document.body.innerHTML;
        var text = document.getElementById('amount').value;
        var text1 = document.getElementById('serv_tax').value;
        var text2 = document.getElementById('tot_amount').value;
        var text3 = document.getElementById('paid_amount').value;
        var text4 = document.getElementById('due_amount').value;
//        console.log("List of values : "+ text + text1 + text2 + text3);

        document.getElementById('amount_display').innerHTML = text;
        document.getElementById('service_tax_display').innerHTML = text1;
        document.getElementById('tot_amount_display').innerHTML = text2;
        document.getElementById('receipt_paymnt').innerHTML = text3;
        document.getElementById('due_amount_display').innerHTML = text4;

        document.getElementById('printableReceiptArea').style.visibility = 'visible';

        var printContents = document.getElementById(divName).innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

</script>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">optical Solution Modal</h4>
            </div>
            <div class="modal-body">
                <input type="button" value="Invoice Print" onclick="printDivInvoice('printableArea')" >
                <input type="button" value="Receipt Print" onclick="printDivReceipt('printableReceiptArea')">
            </div>
            <div class="modal-footer">

            </div>
        </div>

    </div>
</div>