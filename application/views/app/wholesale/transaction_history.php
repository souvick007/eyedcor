<?php $this->load->view('assets/css'); ?>

<?php $this->load->view('assets/js'); ?>


<?php $this->load->view('layouts/header'); ?>

<?php $this->load->model('Postmodel'); ?>


<section>
    <div class="mainwrapper">
        <div class="leftpanel">
<!--            <div id = "myDiv"  class="loader" style="display:none;opacity: 0.5"><img id = "myImage" ></div>-->
            <?php $this->load->view('layouts/mainwholesale'); ?>
        </div><!-- leftpanel -->

        <div class="mainpanel">

            <div class="pageheader">
                <div class="media">
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">TRANSACTION HISTORY</a></li>
                        </ul>
                        <h4>TRANSACTION HISTORY
                        </h4>
                    </div>
                </div><!-- media -->
            </div>
            <br/>

<!--            <input type="button" value="test" onclick="test1()">-->

            <div class="col-lg-12" >
                <div class="medium no-padding" id="gridscroll" >
                    <div style="height: 450px;">
<!--                        <div class="table-responsive" style="overflow: scroll;height: 355px;">-->
                        <div class="table-responsive">
                            <table class="table testing" id="outsourcedatatable">

                                <thead>
                                <tr>
                                    <th class="hath" style="font-size: 11px">INVOICE NO</th>
                                    <th class="hath" style="font-size: 11px">ORDER NO</th>
                                    <th class="hath" style="font-size: 11px">PARTY NAME</th>
                                    <th class="hath" style="font-size: 11px">DATE</th>
                                    <th class="hath" style="font-size: 11px">ITEM ID</th>
                                    <th class="hath" style="font-size: 11px">OTHER SERVICES</th>
                                    <th class="hath" style="font-size: 11px">TOTAL AMOUNT</th>
                                    <th class="hath" style="font-size: 11px">PAID AMOUNT</th>
                                    <th class="hath" style="font-size: 11px">ITEM DUE AMOUNT</th>
                                    <th class="hath" style="font-size: 11px">PRINT OPTION</th>

                                </tr>
                                </thead>
                                <tbody>

                                <?php foreach($transaction_all as $val):?>

                                    <tr style="font-size: 10px;">
                                        <td><?php echo $val->inv_no;?></td>
                                        <td><?php echo $val->ord_no;?></td>
                                        <td><?php echo $val->party_name;?></td>
                                        <td><?php echo $val->updated_on;?></td>
                                        <td><?php echo $val->item_id?></td>
                                        <td><?php echo $val->service_tax?></td>
                                        <td><?php echo $val->total_amount?></td>
                                        <td><?php echo $val->paid_amount?></td>
                                        <td><?php echo $val->item_due_amnt?></td>
                                        <td>
                                            <span style="cursor: pointer;"  onclick="printDivInvoice('printableArea','<?php echo $val->party_name ?>','<?php echo $val->address ?>','<?php echo $val->service_tax ?>','<?php echo $val->total_amount ?>','<?php echo $val->item_id;?>',<?php echo $val->bill_no;?>);">print invoice</span>|
                                            <span style="cursor: pointer;" onclick="printDivReceipt('printableReceiptArea','<?php echo $val->inv_no;?>','<?php echo $val->ord_no;?>','<?php echo $val->party_name;?>','<?php echo $val->service_tax;?>','<?php echo $val->paid_amount;?>','<?php echo $val->due_amount;?>','<?php echo $val->bill_no;?>','<?php echo $val->total_amount;?>')">print receipt</span></td>
                                    </tr>

                                <?php endforeach;?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--    --><?php //$this->load->view('layouts/footer'); ?>
</section>


<div id="printableArea" style="display: none">
    <div class="col-md-12">
        <input type="hidden" id="valtxt">

        <center><div class="txtalgn">RETAIL INVOICE</div></center>
        <center><font size="">ORIGINAL : BUYER'S COPY</font></center>
        <center><font size="">VALID FOR INPUT TAX CREDIT</font></center>
        <h1><center><font size="">EYE DECOR</font></center></h1>
        <center><div class="txtalgn">WHOLE SALE OPTICIAN</div></center>
        <center><div class="txtalgn">S C GOSWAMI ROAD, PANBAJAR</div></center>
        <center><div class="txtalgn">GUWAHATI-781001</div></center>
        <center><div class="txtalgn">VAT NO:18570202998. CST NO 18949944830</div></center>

    </div>
    <div class="col-md-12">

        <style>
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            th, td {
                padding: 15px;
            }
            th {
                text-align: left;
            }
        </style>
        </head>


        <table style="width:100%">

            <tr>
                <td>Buyer's Name :<span id="prty_nme"></span></td>

                <td>TIN :</td>
                <td></td>



            </tr>
            <tr>
                <td>Address :<span id="prty_adrs"></span></td>
                <td>Cst Regd No :</td>
                <td></td>


            </tr>
            <tr>
                <td></td>
                <td>Challan No :<span id="ord_no"</td>
                <td>Date:<?php echo date("d-m-Y")?></td>

            </tr>
            <tr>
                <td>Phone:</td>
                <td>RR/VPP Regd Air No</td>
                <td></td>

            </tr>

        </table>

        <style>
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            th, td {
                padding: 5px;
                text-align: left;
            }
        </style>
        </head>


        <h2></h2>

        <input type="hidden" id="item_id">

        <table style="margin-bottom: 15px !important;width:100%">
            <tr>
                <th><center>SL.NO</center></th>
                <th><center>DC NO</center></th>
                <th><center>REF NO</center></th>
                <th><center>TYPE</center></th>
                <th><center>ITEM</center></th>
                <th><center>Qnty.</center></th>
                <th><center>Specification</center></th>
                <th colspan="4"><center>VALUE OF GOOD QUANTITY PRICE PER UNIT</center></th>

            </tr>
            <tr>

                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <th colspan="2"><center>Amount</center></th>
            </tr>

            <tr>
                <td><?php echo count($this->session->userdata('itemid'));?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <th><center>Rs</center></th>
                <th><center>P</center></th>
            </tr>
            <tr id="txtprint">

            </tr>


            <tr>
                <td rowspan="3"></td>
                <td colspan="4"><center>TOTAL VALUE</center></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><span id="billing_amnt"></span></td>
                <td></td>
            </tr>

            <tr>

                <td colspan="4"><center>FREIGHT & OTHER SERVICES</center></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><span id="othr_srv"></span></td>
                <td></td>
            </tr>


            <tr>
                <td colspan="4"><center>TOTAL PAYBLE AMOUNT</center></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><span id="total_amnt"></span></td>
                <td></td>
            </tr>


        </table>


        Total Amount (in word) Rupees.</br>
        <span id="amnt_in_wrd"></span><br>

        E.&O.E


    </div>

    <div class="col-md-12">

        <div class="col-md-10">
            <div class="">


                <nav>
                    <ul>
                        "I/We hereby certify that my/our registration certificate under the Assam Value Added Tax 2003
                        Is in force on the date on which the sale of the good specified in this tax invoice has been effected
                        By me/us."</br>

                        Goods ordered and not dispatched are out of stock. We are not responsible for any loss or
                        breakage On transit interest @ 18% will be charged if not paid on presentation
                    </ul>
                </nav>
            </div>
        </div>
        <div class="col-md-2">
            <article>
                For EYE DECOR</br></br>

                Authorised Signatory
            </article>
        </div>
    </div>


    <hr style="height:1px; color: #2f363d" />

    <div class="col-md-12">

        <div>
            <tr>
                <td>Buyer's Name :<span id="party_name"></span></td>


            </tr><br>
            <tr>
                <td>Address :<span id="party_address"></span></td>


            </tr>

        </div>

    </div>
</div>

<div id="printableReceiptArea" style="display: none">
    <div class="col-md-12">
        <div class="col-md-5">
            <h1> SL. NO.</h1>

        </div>
        <div class="col-md-7">
            <h2> DATE</h2>
            <h3>GUWAHATI</h3>

        </div>
    </div>
    <div class="col-md-12">






        <div>Received with thanks from M/s._________________________________________________________________</div>
        <div>__________________________________________________________________________________________</div>
        <div>the sum of Rupees___________________________________________________________________________</div>
        <div>__________________________________________________________________________________________</div>
        <div> Only as payment against our sale invoice Nos._____________________________________________________</div>
        <div> by Cash/Cheque* No_________________________________________________________________________</div>
        <div>Dated________________________________ and drawn on__________________________________________</div>
    </div>
    <div class="col-md-12">
        <div class="col-md-3">
            <div><h1>Rs____________</h1></div>
            <div>*All Cheques are subject to realisation</div>
        </div>
        <div class="col-md-2">
            <div><h4>for</h4></div>
        </div>
        <div class="col-md-7">
            <div></div>
            <div><h1>Signature</h1></div>
        </div>
    </div>
</div>







<script type="text/javascript">


</script>

