<?php $this->load->view('assets/css'); ?>

<?php $this->load->view('assets/js'); ?>

<?php $this->load->view('layouts/header'); ?>



<section>
    <div class="mainwrapper">
        <div class="leftpanel">
            <div id = "myDiv"  class="loader" style="display:none;opacity: 0.5"><img id = "myImage" ></div>
            <?php $this->load->view('layouts/mainwholesale'); ?>
        </div><!-- leftpanel -->


        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="media-body">

                        <div id="notification"  style="display: none;position: absolute;top: 3px;right: 1px;width: 28%;z-index: 105;text-align: center;font-size: 14px;font-weight: 700;color: white;background-color: #60b544;padding: 9px;">Data Saved Successfully </div>

                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">PAYMENT </a></li>
                        </ul>
                        <h4>PAYMENT
                        </h4>
                    </div>
                </div><!-- media -->
            </div>
            <br/>

            <div class="col-md-12">

                <div class="col-md-3"><label>Invoice No :<span id="invoice_no"><?php echo $inv_no; ?></span>  </label></div>
                <div class="col-md-3"><label>Party Name : <?php echo $checkprtyname[0]->name; ?> </label></div>
                <div ><input type="hidden" name="hid_prty_name" id="hid_prty_name" value="<?php echo $checkprtyname[0]->name; ?>"/></div>
                <div class="col-md-3"><label>Party Address : <?php echo $checkprtyname[0]->address; ?> </label></div>

            </div>

            <div class="col-md-12" style="border-bottom: dashed 1px black "></div>

            <div class="col-md-12">
                <div class="col-md-3" style="margin-top: 40px">
                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <div class="col-md-8">
                            DUE AMOUNT
                        </div>
                        <div class="col-md-4">
                            <input type="text" name="tot_due_amnt" id="tot_due_amnt"  value="<?php echo $due_amt?>" style="width: 60px"   class="form-control eilmlitecontrols more" disabled>

                        </div>
                    </div>

                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <div class="col-md-8">
                           DISCOUNT
                        </div>
                        <div class="col-md-4">
                            <input type="text" style="width: 60px" name="discnt_amount" id="discnt_amount"  class="form-control eilmlitecontrols more" />
                        </div>
                    </div>

                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <div class="col-md-8">
                            PAID AMOUNT
                        </div>
                        <div class="col-md-4">
                            <input type="text" style="width: 60px" name="payed_amnt" id="payed_amnt"  class="form-control eilmlitecontrols more" />
                        </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="pull-right" style="margin-bottom: 68px;">
                    <input type="button" class="btn btn-primary" id="btnprint" onclick="printDiv('printableArea')" value="Print" />
                </div>
                <div class="pull-right" style="margin-bottom: 68px;">
                    <input type="button" onclick="valsave()" class="btn btn-primary" id="btnsave" value="Save"/>
                </div>
            </div>
        </div>
    </div>

    <div id="printableArea" style="display: none">
        <div style="float: left; width: 20%;">
            <img src="<?php echo base_url(); ?>images/logo1.png" style="max-width: 80%;margin-top: 11px;" />
        </div>
        <div style="float: left; width: 52%;font-size: 41px;margin-top: 21px;">
            Optical Solution
        </div>
        <div style="float:left;margin-top: 17px;margin-left: 10px">

            <strong style="padding:0px 5px 0px 0px">Party Name:</strong><?php echo $checkprtyname[0]->name; ?>&nbsp;&nbsp;

            <strong style="padding:0px 5px 0px 5px">Party Address:</strong><?php echo $checkprtyname[0]->address; ?><br>

            <strong style="padding:0px 5px 0px 5px"> Paid :</strong><span id="paid"></span>

<!--            <strong style="padding:0px 5px 0px 5px">Delivery Date:</strong>--><?php //echo date('Y-m-d'); ?>

        </div>
        <div style="line-height: 40px;">

            <strong style="font-size: 15px;">Invoice No:</strong> <?php echo $inv_no ?>
            <div class="pdname" style="font-size: 18px;">

            </div>
            <br />
        </div>

        <div class="preshrview" style="border: 2px solid green; margin: 15px 0px; float: left; width: 100%;"></div>
        <div>
            <div style="float:right;display:inline-block;margin-right: 200px;margin-top: 10px"><strong>Signature: </strong></div>
        </div>
    </div>


</section>

<script type="text/javascript">
    function printDiv(divName) {

        var originalContents = document.body.innerHTML;


        var text1 = document.getElementById('payed_amnt').value;

        document.getElementById('paid').innerHTML = text1;

        var printContents = document.getElementById(divName).innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>