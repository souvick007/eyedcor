<?php $this->load->view('assets/css'); ?>

<?php $this->load->view('assets/js'); ?>


<?php $this->load->view('layouts/header'); ?>



<section>
    <div class="mainwrapper">
        <div class="leftpanel">
<!--            <div id = "myDiv"  class="loader" style="display:none;opacity: 0.5"><img id = "myImage" ></div>-->
            <?php $this->load->view('layouts/mainwholesale'); ?>
        </div><!-- leftpanel -->

        <?php echo form_open('wholesale/payment');?>

        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">DELIVERY LIST</a></li>
                        </ul>
                        <h4>DELIVERY LIST
                        </h4>
                    </div>
                </div><!-- media -->
            </div>
            <br/>
            <div class="col-md-12">


                <div class="col-md-1 ">
                    Invoice No.
                </div>
                <div class="col-md-2">
                    <input type="number" name="invoice_no" id="invoice_no" class="form-control eilmlitecontrols more">
                    <input type="hidden" name="hidchckval" id="hidchckval">
                </div>

                <?php
                if($this->session->flashdata('messageError') != "")
                {
                    ?>

                    <div class="col-md-2">
                        <input type="checkbox" name="payment[]" onclick="paymentcheck(this)" value="1" <?php echo  set_radio('payment','1',TRUE); ?>/>&nbsp;only payment
                    </div>
                    <div class="col-md-2" id="prty_name_shw" style="display:block" >
                        <input type="text" placeholder="provide party name" name="party_name_prov" id="party_name_prov" />
                        <div><?php echo $this->session->flashdata('messageError'); ?></div>
                    </div>

                    <?php
                }else{ ?>

                    <div class="col-md-2">
                        <input type="checkbox" name="payment[]" id="paymntchck" onclick="paymentcheck(this)"/>&nbsp;only payment
                    </div>
                    <div class="col-md-2" id="prty_name_shw" style="display:none" >
                        <input type="text" placeholder="provide party name" name="party_name_prov" id="party_name_prov"/>
                    </div>

                    <?php } ?>

                <div class="col-md-2">
                    <input type="submit" name="save_generate" id="save_generate" value="save & generate" onclick="return returnvalinvoice()"/>
                </div>
            </div>
            <br/>
            <div class="col-lg-12" style="height:400px;margin-bottom: 56px;overflow-y: scroll;">

                <table class="table" id="deliverylistdatatable">
                    <thead>
                    <tr>
                        <th style="font-size: 11px"><input type="checkbox" name="vehicle" id="select_all" ></th>
                        <th class="hath" style="font-size: 11px">ORDER NO</th>
                        <th class="hath" style="font-size: 11px">BILL NO</th>
                        <th class="hath" style="font-size: 11px">PARTY NAME</th>
                        <th class="hath" style="font-size: 11px">DATE</th>
                        <th class="hath" style="font-size: 11px">PRODUCT TYPE</th>
                        <th class="hath" style="font-size: 11px">PRODUCT NAME</th>
                        <th class="hath" style="font-size: 11px">ITEM DETAILS</th>
                        <th class="hath" style="font-size: 11px" >QUANTITY</th>
                        <th class="hath" style="font-size: 11px" >ORDER STATUS</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach($orderedlist as $val):?>

                        <tr style="font-size: 10px;">
                            <td><input type="checkbox" name="options[]" class="checkbox1" id="delivcheckboxs<?php echo $val->item_id; ?>" onclick="chckbxval('<?php echo $val->item_id; ?>')" value="<?php echo $val->item_id; ?>"></td>
                            <td><?php echo $val->sales_order_id;?></td>
                            <td><?php echo $val->bill_no;?></td>
                            <td><?php echo $val->party_name;?></td>
                            <td><?php echo date('Y-m-d', strtotime($val->order_date));?></td>
                            <td><?php echo $val->product_type?></td>
                            <td><?php echo $val->product_name?></td>
                            <?php if($val->product_type == "READY"){?>
                                <td>sph:<?php echo $val->sph;?>&nbsp;cyl:<?php echo $val->cyl;?>&nbsp;axis:<?php echo $val->axis;?>&nbsp;add:<?php echo $val->addition;?></td>
                            <?php }elseif($val->product_type == "RX"){?>
                                <td>sph:<?php echo $val->sph;?>&nbsp;cyl:<?php echo $val->cyl;?>&nbsp;axis:<?php echo $val->axis;?>&nbsp;add:<?php echo $val->addition;?></td>
                            <?php }elseif($val->product_type == "FITTING"){?>
                                <td>frame:<?php echo $val->frame;?>&nbsp;lens:<?php echo $val->lens;?></td>
                            <?php }elseif($val->product_type == "GRINDING"){?>
                                <td>
                                    sph:<?php echo $val->sph;?>&nbsp;cyl:<?php echo $val->cyl;?>&nbsp;axis:<?php echo $val->axis;?>&nbsp;add:<?php echo $val->addition;?>
                                    dia:<?php echo $val->diameter;?>&nbsp;base:<?php echo $val->base;?>&nbsp;side:<?php echo $val->side;?>&nbsp;remarks:<?php echo $val->remarks;?>
                                </td>
                            <?php }elseif($val->product_type == "CONTACT_LENS"){?>
                                <td>sph:<?php echo $val->sph;?>&nbsp;cyl:<?php echo $val->cyl;?>&nbsp;axis:<?php echo $val->cyl;?>&nbsp;add:<?php echo $val->cyl;?></td>
                            <?php }elseif($val->product_type == "SOLUTION"){?>
                                <td>specification:<?php echo $val->specification;?>&nbsp;remarks:<?php echo $val->remarks;?></td>
                            <?php }elseif($val->product_type == "ACCESSORY"){?>
                                <td></td>
                            <?php }else{?>
                                <td>sph:<?php echo $val->sph;?>&nbsp;cyl:<?php echo $val->cyl;?>&nbsp;axis:<?php echo $val->cyl;?>&nbsp;add:<?php echo $val->cyl;?></td>
                            <?php } ?>
                            <td><?php echo $val->quantity?></td>
                            <td><?php echo $val->status?></td>
                             </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php form_close();?>
    </div>
</section>

<?php $this->load->view('layouts/footer'); ?>


<script>

    var party_name=<?php echo $party_name ?>;

    console.log('Inmsi');
    console.log(party_name);

        $( "#party_name_prov" ).autocomplete({
            source: party_name,
            minLength: 1,
            search: function(oEvent, oUi) {
                // get current input value
                var sValue = $(oEvent.target).val().toUpperCase();
                console.log(sValue);
                // init new search array
                var aSearch = [];
                // for each element in the main array ...
                $(party_name).each(function(iIndex, sElement) {
                    // ... if element starts with input value
                    if (sElement.substr(0, sValue.length) == sValue) {
                        // add element
                        aSearch.push(sElement);
                    }
                });
                // change search array
                $(this).autocomplete('option', 'source', aSearch);
            }
        });

</script>