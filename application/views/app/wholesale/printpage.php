<?php $this->load->view('assets/js'); ?>


<!Doctype html>
<html>
<head>
    <div class="col-md-12">
         <div class="col-md-12">

                 <button onclick="printFunction()"  style="float: right;" id="printpage" >Print this page</button>
         </div>
<!--    <div style="width:100%;height:100%;border:2px solid #000;"><br>-->


        <div style="font-size: 40px;text-align:center">
            <center>Party Name: <?php echo $printdata[0]->party_name;?></center> <br>
            DATE:<?php echo date("Y/m/d");?>
        </div>

        <div style="width:100%;
    margin-top: 34px!important; border:0px solid #000;">

            <style>
                table {
                    font-family: arial, sans-serif;
                    border-collapse: collapse;
                    width: 100%;
                }

                td, th {
                    border: 1px solid #000;
                    text-align: left;
                    padding: 14px;
                    font-weight: normal;

                }


            </style>


        </div>
</head>
<body>

<table >
    <thead>
    <tr>
        <th>DC NO</th>
        <th>REF NO</th>
        <th>PRODUCT NAME</th>

        <th>PRODUCT TYPE</th>
        <th>ITEM DETAILS</th>

        <th>QTY</th>
        <th>AMOUNT</th>
    </tr>
    </thead>

    <tbody>
    <?php foreach($printdata as $val):?>
        <tr>
            <td width=5%><?php echo $val->bill_no;?></td>
            <td width=7%><?php echo $val->ref_no;?></td>
            <td width=12%><?php echo $val->product_name;?></td>
            <td width=15%><?php echo $val->product_type;?></td>

            <?php if($val->product_type == "READY"){?>
                <td>sph:<?php echo $val->sph;?>&nbsp;cyl:<?php echo $val->cyl;?>&nbsp;axis:<?php echo $val->axis;?>&nbsp;add:<?php echo $val->addition;?></td>
            <?php }elseif($val->product_type == "RX"){?>
                <td>sph:<?php echo $val->sph;?>&nbsp;cyl:<?php echo $val->cyl;?>&nbsp;axis:<?php echo $val->axis;?>&nbsp;add:<?php echo $val->addition;?></td>
            <?php }elseif($val->product_type == "FITTING"){?>
                <td>frame:<?php echo $val->frame;?>&nbsp;lens:<?php echo $val->lens;?></td>
            <?php }elseif($val->product_type == "GRINDING"){?>
                <td>
                    sph:<?php echo $val->sph;?>&nbsp;cyl:<?php echo $val->cyl;?>&nbsp;axis:<?php echo $val->axis;?>&nbsp;add:<?php echo $val->addition;?>
                    dia:<?php echo $val->diameter;?>&nbsp;base:<?php echo $val->base;?>&nbsp;side:<?php echo $val->side;?>&nbsp;remarks:<?php echo $val->remarks;?>
                </td>
            <?php }elseif($val->product_type == "CONTACT_LENS"){?>
                <td>sph:<?php echo $val->sph;?>&nbsp;cyl:<?php echo $val->cyl;?>&nbsp;axis:<?php echo $val->cyl;?>&nbsp;add:<?php echo $val->cyl;?></td>
            <?php }elseif($val->product_type == "SOLUTION"){?>
                <td>specification:<?php echo $val->specification;?>&nbsp;remarks:<?php echo $val->remarks;?></td>
            <?php }elseif($val->product_type == "ACCESSORY"){?>
                <td></td>
            <?php }else{?>
                <td>sph:<?php echo $val->sph;?>&nbsp;cyl:<?php echo $val->cyl;?>&nbsp;axis:<?php echo $val->cyl;?>&nbsp;add:<?php echo $val->cyl;?></td>
            <?php } ?>

            <td width=5%><?php echo $val->quantity;?>
            <td width=12%><?php echo $val->price;?></td>
        </tr>
    <?php endforeach;?>
    </tbody>
</table>


</body>


</html>
</div>
