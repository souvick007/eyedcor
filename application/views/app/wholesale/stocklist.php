<?php $this->load->view('assets/css'); ?>

<?php $this->load->view('assets/js'); ?>


<?php $this->load->view('layouts/header'); ?>



<section>
    <div class="mainwrapper">
        <div class="leftpanel">
            <div id = "myDiv"  class="loader" style="display:none;opacity: 0.5"><img id = "myImage" ></div>
            <?php $this->load->view('layouts/mainwholesale'); ?>
        </div><!-- leftpanel -->

        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">STOCKLIST</a></li>
                        </ul>
                        <h4>STOCKLIST
                        </h4>
                    </div>
                </div><!-- media -->
            </div>
            <br/>

                    <input type="text" id="prod_name" class="widthstyle" placeholder="product name" style="margin-left: 10px;margin-right: 5px">
                    <input type="text" class="widthstyle" id="sph" placeholder="sph" style="margin-right: 5px">
                    <input type="text" class="widthstyle" id="cyl" placeholder="cyl" style="margin-right: 5px">
                    <input type="text" class="widthstyle" id="axis" placeholder="axis" style="margin-right: 5px">
                    <input type="text" class="widthstyle" id="addition" placeholder="addition" style="margin-right: 5px">
                    <input type="button" value="search" onclick="searchlist()">
<!--                    Email: <input type="text"><br>-->
<!--                    Date of birth: <input type="text">-->

            <div class="col-lg-12" style="height: 450px;">
                <div class="medium no-padding" id="gridscroll">
                    <table class="table" id="stocklistdatatable">
                        <thead>
                        <tr>
                            <th class="hath" style="font-size: 11px">PRODUCT NAME</th>
                            <th class="hath" style="font-size: 11px">SPH</th>
                            <th class="hath" style="font-size: 11px">CYL</th>
                            <th class="hath" style="font-size: 11px">AXIS</th>
                            <th class="hath" style="font-size: 11px">ADDITION</th>
                            <th class="hath" style="font-size: 11px" >QUANTITY</th>

                        </tr>
                        </thead>
                        <tbody id="gettingstocklist">
                        <div id = "myDiv"  class="loader" style="display:none;opacity: 0.5"></div>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>



</section>

<?php $this->load->view('layouts/footer'); ?>