<?php $this->load->view('assets/css'); ?>

<?php $this->load->view('assets/js'); ?>


<?php $this->load->view('layouts/header'); ?>



<section>
    <div class="mainwrapper">
        <div class="leftpanel">
            <div id = "myDiv"  class="loader" style="display:none;opacity: 0.5"><img id = "myImage" ></div>
            <?php $this->load->view('layouts/mainwholesale'); ?>
        </div><!-- leftpanel -->

        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">OUT SOURCE LIST</a></li>
                        </ul>
                        <h4>OUT SOURCE LIST
                        </h4>
                    </div>
                </div><!-- media -->
            </div>
            <br/>


            <?php echo form_open('wholesale/change_status_outsource');?>

            <div class="col-md-12" style="margin-bottom: 9px!important;">




                <div class="col-md-3" style=" margin-right: -141px;">

                    <select style=" font-size: 17px;" name="selectstat">
                        <option value="Out Source">Out Source</option>
                        <option value="Received">Received</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <input type="submit" name="save" id="save" value="save">




                </div>


            </div>

            <div class="col-lg-12" >

                <div class="medium no-padding" id="gridscroll">
                    <div style="height: 450px;">
                        <div class="table-responsive ">
                            <table class="table" id="outsourcedatatable">
                                <thead>
                                <tr>
                                    <th style="font-size: 11px"><input type="checkbox" name="vehicle" id="select_all" ></th>
                                    <th class="hath" style="font-size: 11px">ORDER NO</th>
                                    <th class="hath" style="font-size: 11px">BILL NO</th>
                                    <th class="hath" style="font-size: 11px">PARTY NAME</th>
                                    <th class="hath" style="font-size: 11px">DATE</th>
                                    <th class="hath" style="font-size: 11px">PRODUCT TYPE</th>
                                    <th class="hath" style="font-size: 11px">PRODUCT NAME</th>
                                    <th class="hath" style="font-size: 11px">ITEM DETAILS</th>
                                    <th class="hath" style="font-size: 11px" >QUANTITY</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php foreach($outsourcelist as $val):?>

                                    <tr style="font-size: 10px;">

                                        <td><input type="checkbox" class="checkbox1" name="select_check[]" value="<?php echo $val->item_id;?>"></td>

                                        <td><?php echo $val->sales_order_id;?></td>
                                        <td><?php echo $val->bill_no;?></td>
                                        <td><?php echo $val->party_name;?></td>
                                        <td><?php echo date('Y-m-d', strtotime($val->order_date));?></td>
                                        <td><?php echo $val->product_type?></td>
                                        <td><?php echo $val->product_name?></td>
                                        <?php if($val->product_type == "FITTING"){?>
                                            <td>frame:<?php echo $val->frame;?>&nbsp;lens:<?php echo $val->lens;?></td>
                                        <?php }elseif($val->product_type == "GRINDING"){?>
                                            <td>
                                                sph:<?php echo $val->sph;?>&nbsp;cyl:<?php echo $val->cyl;?>&nbsp;axis:<?php echo $val->axis;?>&nbsp;add:<?php echo $val->addition;?><br>
                                                dia:<?php echo $val->diameter;?>&nbsp;base:<?php echo $val->base;?>&nbsp;side:<?php echo $val->side;?>&nbsp;remarks:<?php echo $val->remarks;?>
                                            </td>
                                        <?php }else{ ?>
                                            <td></td>
                                        <?php } ?>
                                        <td><?php echo $val->quantity?></td>
                                    </tr>

                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <?php echo form_close();?>
        </div>
    </div>

    <?php $this->load->view('layouts/footer'); ?>

</section>