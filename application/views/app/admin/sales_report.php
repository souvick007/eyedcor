<?php $this->load->view('assets/css'); ?>
<?php $this->load->view('assets/js'); ?>

<?php $this->load->view('layouts/header'); ?>



<section>
    <div class="mainwrapper">
        <div class="leftpanel">
            <div id = "myDiv"  class="loader" style="display:none;opacity: 0.5"></div>
            <?php $this->load->view('layouts/mainadmin'); ?>
        </div>

        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">SALES REPORT</a></li>
                        </ul>
                        <h4>SALES REPORT
                        </h4>
                    </div>
                </div>
            </div>
            <br/>

            <?php echo form_open('admin/sales_report');?>

            <div class="col-md-12">

                <label >FROM</label>
                <input type="date" value="<?php echo $from_date?>"  name="from_date" id="from_date">


                <label >TO</label>

                <input type="date" value="<?php echo $to_date?>" name="to_date" id="to_date">



                <label>Payment Mode</label>
                <select name="selectpaymentmode" id="selectpaymentmode">
                    <option value="all">All</option>
                    <option value="cash">Cash</option>
                    <option value="cheque">Cheque</option>
                </select>


<!--                <div class="form-group ">-->
<!--                    <label for="lg">PARTY NAME</label>-->
<!--                    <div>-->
<!--                        <input class="form-control" type="text" onblur="partynamechecksalesreport()" id="party_name" name="party_name" placeholder="PARTY NAME">-->
<!---->
<!--                    </div>-->
<!--                </div>-->

<!---->
               <label>Party Name</label>
            <input type="text" name="party_name" style="width:8%"  id="party_name" onblur="partynamechecksalesreport()" >
                <label>Product Type</label>
                <select name="selectproducttype" id="selectproducttype">
                    <option value="">SELECT TYPE</option>
                    <option value="READY">READY</option>
                    <option value ="RX">RX</option>
                    <option value ="GRINDING">GRINDING</option>
                    <option value ="CONTACT_LENS">CONTACT LENS</option>
                    <option value ="SOLUTION">SOLUTION</option>
                    <option value ="ACCESSORY">ACCESSORY</option>
                    <option value ="FITTING">FITTING</option>
                </select>
                <label>Bill No</label>
                <input type="text" name="bill_no" id="bill_no"style="width:6%">

                <input type="submit" name="report" id="report" value="REPORT">
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</section>
<script>
    var party_name=<?php echo $party_name?>;

    console.log("hiii");

    console.log("Sales Party Name");
    console.log(party_name);

    $( "#party_name" ).autocomplete({
        source: party_name,
        minLength: 1,
        search: function(oEvent, oUi) {
// get current input value
            var sValue = $(oEvent.target).val().toUpperCase();
            console.log(sValue);
// init new search array
            var aSearch = [];
// for each element in the main array ...
            $(party_name).each(function(iIndex, sElement) {
// ... if element starts with input value
                if (sElement.substr(0, sValue.length) == sValue) {
// add element
                    aSearch.push(sElement);
                }
            });
// change search array
            $(this).autocomplete('option', 'source', aSearch);
        }
    });
</script>

