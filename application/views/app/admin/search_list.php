<<?php $this->load->view('assets/css'); ?>

<?php $this->load->view('assets/js'); ?>


<?php $this->load->view('layouts/header'); ?>



<section>
    <div class="mainwrapper">
        <div class="leftpanel">
            <div id = "myDiv"  class="loader" style="display:none;opacity: 0.5"></div>
            <?php $this->load->view('layouts/mainadmin'); ?>
        </div><!-- leftpanel -->

        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">SEARCH LIST</a></li>
                        </ul>
                        <h4>SEARCH LIST
                        </h4>
                    </div>
                </div><!-- media -->
            </div>
            <br/>

               <?php echo form_open('admin/search_list');?>
<!--            <div class="col-lg-7" >-->

               <div class="col-lg-5">
                   <label class="col-sm-2 control-label">FROM</label>
                   <input type="date" value="<?php echo $from_date?>"  name="from_date" id="from_date">
                   </div>
<!--            <div class="col-lg-5">-->
                <div class="col-lg-5" style="margin-left: -200px;">




                <label class="col-sm-2 control-label">TO</label>

                <input type="date" value="<?php echo $to_date?>" name="to_date" id="to_date">
                <input type="submit" name="go" id="go" value="GO">
            </div>



<!--                <div class="col-lg-7" style="margin-left: 693px;">-->
<!---->
<!---->
<!---->
<!---->
<!--                <input id="order_date" name="order_date" type="text" placeholder="search">-->
<!--                <input type="submit"  value="search">-->
<!--              </div>-->


                    <div class="col-lg-12" >
                        <div class="medium no-padding" id="gridscroll" >
                            <div style="height: 450px;">
                                <div class="table-responsive ">
                                    <table class="table display" id="salesreportdatatable">
<!--                                        <table id="example" class="display" cellspacing="0" width="100%">-->

                                        <thead>
                                        <tr>



                                            <th class="hath" style="font-size: 11px">ORDER NO</th>
                                            <th class="hath" style="font-size: 11px">BILL NO</th>
                                            <th class="hath" style="font-size: 11px">PARTY NAME</th>
                                            <th class="hath" style="font-size: 11px">DATE</th>
                                            <th class="hath" style="font-size: 11px">PRODUCT TYPE</th>
                                            <th class="hath" style="font-size: 11px">PRODUCT NAME</th>
                                            <th class="hath" style="font-size: 11px">ITEM DETAILS</th>
                                            <th class="hath" style="font-size: 11px">STATUS</th>
                                            <th class="hath" style="font-size: 11px">REFERENCE NUMBER</th>
                                            <th class="hath" style="font-size: 11px" >QUANTITY</th>
                                            <!--                                    <th class="hath" style="font-size: 11px" >ORDER STATUS</th>-->
                                            <!--                                    <th class="hath" style="font-size: 11px" >SAVE STATUS</th>-->
                                        </tr>
                                        </thead>



                                        <tfoot>
                                        <tr>
                                            <th class="hath" style="font-size: 11px">ORDER NO</th>
                                            <th class="hath" style="font-size: 11px">BILL NO</th>
                                            <th class="hath" style="font-size: 11px">PARTY NAME</th>
                                            <th class="hath" style="font-size: 11px">DATE</th>
                                            <th class="hath" style="font-size: 11px">PRODUCT TYPE</th>
                                            <th class="hath" style="font-size: 11px">PRODUCT NAME</th>
                                            <th class="hath" style="font-size: 11px">ITEM DETAILS</th>
                                            <th class="hath" style="font-size: 11px">STATUS</th>
                                            <th class="hath" style="font-size: 11px">REFERENCE NUMBER</th>
                                            <th class="hath" style="font-size: 11px" >QUANTITY</th>
                                        </tr>
                                        </tfoot>

                                        <tbody style="font-size: 11px;">



                                        <?php foreach($getsalesorderitemdetail as $val):?>
                                            <tr>

                                                <td><?php echo $val->sales_id;?></td>
                                                <td><?php echo $val->bill_no;?></td>
                                                <td><?php echo $val->party_name;?></td>
                                                <td><?php echo date('d M y', strtotime($val->order_date));?></td>
                                                <td><?php echo $val->product_type;?></td>
                                                <td><?php echo $val->product_name;?></td>

                                                <?php if($val->product_type == "READY"){ ?>
                                                    <td><b>sph:</b><?php echo $val->sph;?>&nbsp;<b>cyl:</b><?php echo $val->cyl;?>&nbsp;<b>axis:</b><?php echo $val->axis;?>&nbsp;<b>addition:</b><?php echo $val->addition;?>&nbsp;<b>side:</b><?php echo $val->side;?><br><b>remarks:</b><?php echo $val->remarks;?></td>
                                                <?php }else if($val->product_type == "RX"){ ?>
                                                    <td><b>sph:</b><?php echo $val->sph;?>&nbsp;<b>cyl:</b><?php echo $val->cyl;?>&nbsp;<b>axis:</b><?php echo $val->axis;?>&nbsp;<b>addition:</b><?php echo $val->addition;?>&nbsp;<b>side:</b><?php echo $val->side;?><br><b>spec:</b><?php echo $val->specification;?><br><b>remarks:</b><?php echo $val->remarks;?></td>
                                                <?php }else if($val->product_type == "GRINDING"){ ?>
                                                    <td><b>sph:</b><?php echo $val->sph;?>&nbsp;<b>cyl:</b><?php echo $val->cyl;?>&nbsp;<b>axis:</b><?php echo $val->axis;?>&nbsp;<b>addition:</b><?php echo $val->addition;?><br><b>side:</b><?php echo $val->side;?>&nbsp;<b>dia:</b><?php echo $val->diameter;?>&nbsp;<b>base:</b><?php echo $val->base;?><br><?php $val->remarks; ?></td>
                                                <?php }else if($val->product_type == "CONTACT_LENS"){ ?>
                                                    <td><b>sph:</b><?php echo $val->sph;?>&nbsp;<b>cyl:</b><?php echo $val->cyl;?>&nbsp;<b>axis:</b><?php echo $val->axis;?>&nbsp;<b>addition:</b><?php echo $val->addition;?>&nbsp;<b>side:</b><?php echo $val->side;?><br><b>spec:</b><?php echo $val->specification;?><br><b>remarks:</b><?php echo $val->remarks;?></td>
                                                <?php }else if($val->product_type == "SOLUTION"){ ?>
                                                    <td><b>remarks:</b><?php $val->remarks; ?></td>
                                                <?php }else if($val->product_type == "ACCESSORY"){ ?>
                                                    <td><b>remarks:</b><?php $val->remarks; ?></td>
                                                <?php }else if($val->product_type == "FITTING"){ ?>
                                                    <td><b>frame:</b><?php $val->frame; ?>&nbsp;<b>lens:</b><?php $val->lens; ?>&nbsp;<b>remarks:</b><?php $val->remarks; ?></td>
                                                <?php }else{ ?>
                                                <?php } ?>
                                                <td><?php echo $val->status;?></td>
                                                <td><?php echo $val->ref_no;?></td>
                                                <td><?php echo $val->quantity;?></td>



                                            </tr>

                                        <?php endforeach;?>
                                     </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php echo form_close();?>

    </div>



