<?php $this->load->view('assets/css'); ?>

<?php $this->load->view('assets/js'); ?>


<?php $this->load->view('layouts/header'); ?>



<section>
    <div class="mainwrapper">
        <div class="leftpanel">
            <div id = "myDiv"  class="loader" style="display:none;opacity: 0.5"><img id = "myImage" ></div>
            <?php $this->load->view('layouts/mainadmin'); ?>
        </div><!-- leftpanel -->

        <div class="mainpanel">

            <div class="pageheader">
                <div class="media">
                    <div class="media-body">

                        <?php
                        if($this->session->flashdata('messageError') != "")
                        {
                        ?>
                        <div id="notification"  style="display: none;position: absolute;top: 3px;right: 1px;width: 28%;z-index: 105;text-align: center;font-size: 14px;font-weight: 700;color: white;background-color: #60b544;padding: 9px;"><?php echo $this->session->flashdata('messageError'); ?></div>
                        <?php } ?>

                        <?php
                        if($this->session->flashdata('messageSuccess') != "")
                        {
                         ?>
                            <div id="notification"  style="display: none;position: absolute;top: 3px;right: 1px;width: 28%;z-index: 105;text-align: center;font-size: 14px;font-weight: 700;color: white;background-color: #60b544;padding: 9px;"><?php echo $this->session->flashdata('messageSuccess'); ?></div>
                        <?php
                        }
                        ?>


                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">DELIVERY HISTORY</a></li>
                        </ul>
                        <h4>DELIVERY HISTORY
                        </h4>
                    </div>
                </div><!-- media -->
            </div>
            <br/>


            <?php echo form_open('admin/del_deliv_data');?>


            <div class="col-lg-12" >
                <div class="medium no-padding" id="gridscroll">
                    <div style="height: 450px;">
                        <div class="table-responsive ">
                            <div><input type="submit" name="delete" id="delete" value="Delete">

                            <input type="submit" name="save" id="save" value="Save">

                                <select name="selectstatus">
                                    <option value="delivered">Delivered</option>
                                    <option value="return">Return</option>

                                </select>
                            </div>
                            <table class="table testing" id="admindelivlist">
                                <thead>
                                <tr>
                                    <th style="font-size: 11px"><input type="checkbox" name="vehicle" id="select_all"></th>
                                    <th style="font-size: 11px">ORDER NO</th>
                                    <th style="font-size: 11px">BILL NO</th>
                                    <th style="font-size: 11px">PARTY NAME</th>
                                    <th style="font-size: 11px">DATE</th>
                                    <th style="font-size: 11px">PRODUCT TYPE</th>
                                    <th style="font-size: 11px">PRODUCT NAME</th>
                                    <th style="font-size: 11px">ITEM DETAILS</th>
                                    <th style="font-size: 11px" >ORDER STATUS</th>
                                    <th style="font-size: 11px" >QUANTITY</th>

                                </tr>
                                </thead>
                                <tbody>

                                <?php foreach($delivlist as $val):?>

                                    <tr style="font-size: 10px;">

                                        <td><input type="checkbox" class="checkbox1" name="select_check[]" value="<?php echo $val->item_id;?>,<?php echo $val->product_type;?>,<?php echo $val->product_name;?>,<?php echo $val->sph;?>,<?php echo $val->cyl;?>,<?php echo $val->axis;?>,<?php echo $val->addition;?>,<?php echo $val->quantity;?>"></td>
<!---->
<!--                                        <td><input type="checkbox" class="checkbox1" name="select_check[]" value="--><?php //echo $val->item_id;?><!--,--><?php //echo $val->product_type;?><!--,--><?php //echo $val->product_name;?><!--,--><?php //echo $val->sph;?><!--,--><?php //echo $val->cyl;?><!--,--><?php //echo $val->axis;?><!--,--><?php //echo $val->addition;?><!--,--><?php //echo $val->quantity;?><!--"></td>-->

                                        <td><?php echo $val->sales_order_id;?></td>
                                        <td><?php echo $val->bill_no;?></td>
                                        <td><?php echo $val->party_name;?></td>
                                        <td><?php echo date('d M y', strtotime($val->order_date));?></td>
                                        <td><?php echo $val->product_type?></td>
                                        <td><?php echo $val->product_name?></td>
                                        <?php if($val->product_type == "READY"){ ?>
                                            <td><b>sph:</b><?php echo $val->sph;?>&nbsp;<b>cyl:</b><?php echo $val->cyl;?>&nbsp;<b>axis:</b><?php echo $val->axis;?>&nbsp;<b>addition:</b><?php echo $val->addition;?>&nbsp;<b>side:</b><?php echo $val->side;?><br><b>remarks:</b><?php echo $val->remarks;?></td>
                                        <?php }else if($val->product_type == "RX"){ ?>
                                            <td><b>sph:</b><?php echo $val->sph;?>&nbsp;<b>cyl:</b><?php echo $val->cyl;?>&nbsp;<b>axis:</b><?php echo $val->axis;?>&nbsp;<b>addition:</b><?php echo $val->addition;?>&nbsp;<b>side:</b><?php echo $val->side;?><br><b>spec:</b><?php echo $val->specification;?><br><b>remarks:</b><?php echo $val->remarks;?></td>
                                        <?php }else if($val->product_type == "GRINDING"){ ?>
                                            <td><b>sph:</b><?php echo $val->sph;?>&nbsp;<b>cyl:</b><?php echo $val->cyl;?>&nbsp;<b>axis:</b><?php echo $val->axis;?>&nbsp;<b>addition:</b><?php echo $val->addition;?><br><b>side:</b><?php echo $val->side;?>&nbsp;<b>dia:</b><?php echo $val->diameter;?>&nbsp;<b>base:</b><?php echo $val->base;?><br><?php $val->remarks; ?></td>
                                        <?php }else if($val->product_type == "CONTACT_LENS"){ ?>
                                            <td><b>sph:</b><?php echo $val->sph;?>&nbsp;<b>cyl:</b><?php echo $val->cyl;?>&nbsp;<b>axis:</b><?php echo $val->axis;?>&nbsp;<b>addition:</b><?php echo $val->addition;?>&nbsp;<b>side:</b><?php echo $val->side;?><br><b>spec:</b><?php echo $val->specification;?><br><b>remarks:</b><?php echo $val->remarks;?></td>
                                        <?php }else if($val->product_type == "SOLUTION"){ ?>
                                            <td><b>remarks:</b><?php $val->remarks; ?></td>
                                        <?php }else if($val->product_type == "ACCESSORY"){ ?>
                                            <td><b>remarks:</b><?php $val->remarks; ?></td>
                                        <?php }else if($val->product_type == "FITTING"){ ?>
                                            <td><b>frame:</b><?php $val->frame; ?>&nbsp;<b>lens:</b><?php $val->lens; ?>&nbsp;<b>remarks:</b><?php $val->remarks; ?></td>
                                        <?php }else{ ?>
                                        <?php } ?>
                                        <td><?php echo $val->status?></td>
                                        <td><?php echo $val->quantity?></td>


                                    </tr>

                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <?php echo form_close();?>


        </div>
    </div>
    <?php $this->load->view('layouts/footer'); ?>
</section>

