<?php $this->load->view('assets/css'); ?>

<?php $this->load->view('assets/js'); ?>


<?php $this->load->view('layouts/header'); ?>



<section>
    <div class="mainwrapper">
        <div class="leftpanel">
            <div id = "myDiv"  class="loader" style="display:none;opacity: 0.5"></div>
            <?php $this->load->view('layouts/mainadmin'); ?>
        </div><!-- leftpanel -->

        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">PRODUCT NAME</a></li>
                        </ul>
                        <h4 class="headingstyle">PRODUCT NAME(ACCESSORY)
                        </h4>
                    </div>
                </div><!-- media -->
            </div>
            <br/>

            <div class="col-lg-5">
                <div class="form-horizontal">

                    <?php echo form_open('admin/product_name_save_acc')?>

                    <div class="form-group ">
                        <label class="col-sm-4 control-label">PRODUCT NAME</label>
                        <div class="col-sm-8">
                            <input class="form-control"  name="product_name" id="product_name" type="text">
                            <input class="form-control"  name="product_name_id"  id="product_name_id" type="hidden">
                        </div>
                    </div>
                    <div class="pull-right" style="margin-bottom: 68px;">
                        <input type="submit" name="save" class="btn btn-primary" id="btnsave" value="Save"/>
                    </div>

                    <?php echo form_close();?>

                </div>
            </div>

            <div class="col-lg-7" >
                <div class="medium no-padding searchpositioning"  style="overflow-y: auto;">
                    <div >
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="font-size: 11px">Sl. No</th>
                                <th style="font-size: 11px">PRODUCT NAME</th>
                                <th style="font-size: 11px">ACTION</th>
                            </tr>
                            </thead>
                            <tbody>

                            <tbody style="background: white;" id="listview">
                            <?php foreach($productname as $key => $val):?>
                                <tr>
                                    <td><?php echo $key + 1;?></td>
                                    <td><?php echo $val->prod_name; ?></td>
                                    <td><a onclick="gettingprodname('<?php echo $val->prod_name; ?>','<?php echo $val->prod_name_id; ?>')"><i class="fa fa-pencil-square-o" style="cursor: pointer;" aria-hidden="true"></i></a>&nbsp;|&nbsp;<a href="<?php echo base_url();?>admin/del_prod_name/<?php echo $val->prod_name_id;?>"><i class="fa fa-remove" style="cursor: pointer;color: #2a6188;" aria-hidden="true"></i></a></td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

