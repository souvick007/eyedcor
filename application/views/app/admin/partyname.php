<?php $this->load->view('assets/css'); ?>

<?php $this->load->view('assets/js'); ?>


<?php $this->load->view('layouts/header'); ?>



<section>
    <div class="mainwrapper">
        <div class="leftpanel">
            <div id = "myDiv"  class="loader" style="display:none;opacity: 0.5"></div>
            <?php $this->load->view('layouts/mainadmin'); ?>
        </div><!-- leftpanel -->

        <div class="mainpanel">
            <div class="pageheader">
                <div class="media">
                    <div class="media-body">
                        <ul class="breadcrumb">
                            <li><a href=""><i class="glyphicon glyphicon-home"></i></a></li>
                            <li><a href="">PARTY NAME</a></li>
                        </ul>
                        <h4>PARTY NAME
                        </h4>
                    </div>
                </div><!-- media -->
            </div>
            <br/>

            <div class="col-lg-5">
                <div class="form-horizontal">

                    <?php echo form_open('admin/party_name_save')?>

                    <div class="form-group ">
                        <label class="col-sm-4 control-label">PARTY NAME</label>
                        <div class="col-sm-8">
                            <input class="form-control"  name="party_name" id="party_name" type="text">
                            <input class="form-control"  name="party_name_id"  id="party_name_id" type="hidden">
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-4 control-label">PARTY ADDRESS</label>
                        <div class="col-sm-8">
                            <textarea class="form-control"  name="party_address" id="party_address"></textarea>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-4 control-label">OPENING BALANCE</label>
                        <div class="col-sm-8">
                            <input class="form-control"  name="opening_bal" id="opening_bal" type="text">
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="col-sm-4 control-label">TIN</label>
                        <div class="col-sm-8">
                            <input class="form-control"  name="tin_no" id="tin_no" type="number">
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="col-sm-4 control-label">Cst Regd No</label>
                        <div class="col-sm-8">
                            <input class="form-control"  name="cst_regd_no" id="cst_regd_no" type="number">
                        </div>
                    </div>

                    <div class="pull-right" style="margin-bottom: 68px;">
                        <input type="submit" name="save" class="btn btn-primary" id="btnsave" value="Save"/>
                    </div>

                    <?php echo form_close();?>

                </div>
            </div>

            <div class="col-lg-7" >
                <div class="medium no-padding searchpositioning scrolltable" >
                        <table class="table">
                            <thead>
                            <tr>
                                <th style="font-size: 11px">Sl. No</th>
                                <th style="font-size: 11px">PARTY NAME</th>
                                <th style="font-size: 11px">ADDRESS</th>
                                <th style="font-size: 11px">OPENING BALANCE</th>
                                <th style="font-size: 11px">ACTION</th>
                            </tr>
                            </thead>
                            <tbody>

                            <tbody style="background: white;" id="listview">
                            <?php foreach($partyname as $key => $val):?>
                                <tr>
                                    <td><?php echo $key + 1;?></td>
                                    <td><?php echo $val->name; ?></td>
                                    <td><?php echo $val->address; ?></td>
                                    <td><?php echo $val->due_amount; ?></td>
                                    <td><a onclick="gettingpartyname('<?php echo $val->id; ?>','<?php echo $val->name; ?>','<?php echo $val->address; ?>','<?php echo $val->due_amount; ?>','<?php echo $val->tin; ?>','<?php echo $val->cst_regd_no; ?>')"><i class="fa fa-pencil-square-o" style="cursor: pointer;" aria-hidden="true"></i></a>&nbsp;|&nbsp;<a href="<?php echo base_url();?>admin/del_party_name/<?php echo $val->id;?>"><i class="fa fa-remove" style="cursor: pointer;color: #2a6188;" aria-hidden="true"></i></a></td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</section>
