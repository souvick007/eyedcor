<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//adding config items.

$config['prod_name'] = array (
    'SINGLE VISION UC',
    'SINGLE VISION HC',
    'SINGLE VISION ARC',
    'PG SINGLE VISION',
    'PB SINGLE VISION',
    'SINGLE VISION CROSS COMP',
    'KT UC',
    'KT HC',
    'COMP KT HC',
    'ARC KT',
    'ARC COMP KT',
    'PG KT',
    'PG COMP KT',
    'PG ARC KT',
    'PB KT',
    'PB COMP KT',
    'PB ARC KT',
    'PROGRESSIVE UC',
    'PROGRESSIVE HC',
    'PROGRESSIVE ARC',
    'PROGRESSIVE PG',
    'PROGRESSIVE PG ARC',
    'PROGRESSIVE PB',
    'PROGRESSIVE PB ARC',
);

$config['cont_prod_name'] = array (
    'BIOTRUE ONE DAY',
    'SOFT LENS DAILY DISPOSABLE TORIC',
    'PURE VISION 2',
    'SOFT LENS DAILY DISPOSABLE TORIC',
    'SOFT LENS TORIC',
    'PURE VISION 2(HD)',
    'SOFT LENS MULTIFOCAL',
    'PURE VISION 2(PRESBYOPIA)',
    'OPTIMA NATURAL LOOK',
    'B/U/HO',
    'I CONNECT',
    'LACELLE COLOR',
);

$config['cont_spec'] = array (
    'DAILY/ 30 PER BOX',
    'DAILY/ 30 or 90 PER BOX',
    'MONTHLY 6 PER BOX',
    'DAILY 30 PER BOX',
    'MONTHLY 1/ 6 PER BOX',
    'QUARTERLY 1 PER BOX',
    'YEARLY 1 PER BOX',
    'MONTHLY 1 PER BOX',
);

$config['soln_prod_name'] = array (
    'RENU 60 Ml',
    'RENU 120 Ml',
    'MONTHLY 6 PER BOX',
    'RENU 355 Ml',
    'RENU 500 Ml',
    'BIO TRUE 60 Ml',
    'BIO TRUE 300 Ml',
    'BIO TRUE 120 Ml',
);

$config['grind_prod_name'] = array (
    'WT SV GLASS',
    'PG SV GLASS',
    'PB SV GLASS',
    'SP2 SV GLASS',
    'B2 SV GLASS',
    'WT KB GLASS',
    'PG KB GLASS',
    'PB KB GLASS',
    'SP2 KB GLASS',
);

?>
